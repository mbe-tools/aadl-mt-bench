package tests.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import tests.TestSuiteYAMTLAbstract;

public class TestSuiteCopying extends TestSuiteYAMTLAbstract {

	public TestSuiteCopying() {
		super();
	}

	public static void main(String[] args) {
		runAllTests( args, new TestSuiteCopying() );
	}

	@Override
	protected String getTestSuiteName() {
		return "YAMTL-Copying";
	}

	@Override
	protected AbstractScenario getScenario( ScenarioKind scenarioKind) {
		switch (scenarioKind) {
			case ADDITION:
				return new CopyingAddition();
	
			case UPDATE_ATT:
				return new CopyingUpdate();
	
			case UPDATE_REF:
				return new CopyingUpdateReference();
			
			case DELETE:
				return new CopyingDeletion();
			
			case BATCH:
				return new CopyingBatch();
	
			default:
				throw new IllegalArgumentException( "Unknown scenario kind: '" + scenarioKind + "'." );
		}
	}
}