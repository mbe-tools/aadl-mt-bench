package tests.copying;

import aadl2aadl.AbstractTransformation;
import aadl2aadl.Copying;
import tests.AbstractScenarioBatch;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;

public class CopyingBatch extends AbstractScenarioBatch {

	public CopyingBatch() {
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.COPY_BATCH_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Copying();
	}

}
