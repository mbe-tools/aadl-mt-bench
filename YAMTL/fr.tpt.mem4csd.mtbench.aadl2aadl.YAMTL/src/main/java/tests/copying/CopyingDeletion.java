package tests.copying;

import aadl2aadl.AbstractTransformation;
import aadl2aadl.Copying;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
public class CopyingDeletion extends AbstractScenarioSync {

	public CopyingDeletion() {
	}
	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.DELETE;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.COPY_DELETION_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Copying();
	}
}
