package tests.copying;

import aadl2aadl.AbstractTransformation;
import aadl2aadl.Copying;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingUpdate extends AbstractScenarioSync {

	public CopyingUpdate() {
	}
	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_ATT;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.COPY_UPDATE_ATT_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Copying();
	}
}
