package tests.refinement;

import aadl2aadl.Aadl2Aadl;
import aadl2aadl.AbstractTransformation;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementUpdateAttribute extends AbstractScenarioSync {

	public RefinementUpdateAttribute() {
	}

	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_ATT;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.REFINEMENT_UPDATE_ATT_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Aadl2Aadl();
	}

}
