package aadl2aadl

import java.util.List

import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionInstanceEnd
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.SystemInstance
import yamtl.dsl.Rule

//5 rules with one input element and one output element to copy the AADL instance model
class Copying extends AbstractTransformation{


	new() {
		header().in('in', INSTANCE).out('out', INSTANCE)
		ruleStore(#[
			new Rule('connection2connection').in('connection_in', INSTANCE.connectionInstance).build.out(
				'connection_out', INSTANCE.connectionInstance, [
					val connection_out = 'connection_out'.fetch as ConnectionInstance
					val connection_in = 'connection_in'.fetch as ConnectionInstance
					connection_out.name = connection_in.name
					connection_out.kind = connection_in.kind
					connection_out.bidirectional = connection_in.bidirectional
					connection_out.complete = connection_in.complete

					connection_out.source = connection_in.source.fetch as ConnectionInstanceEnd
					connection_out.destination = connection_in.destination.fetch as ConnectionInstanceEnd

					connection_out.connectionReferences +=
						(connection_in.connectionReferences.fetch as List<ConnectionReference>).filterNull

				]).build.build,
			new Rule('feature2feature').in('feature_in', INSTANCE.featureInstance).build.out('feature_out',
				INSTANCE.featureInstance, [
					val feat_out = 'feature_out'.fetch as FeatureInstance
					val feat_in = 'feature_in'.fetch as FeatureInstance

					feat_out.category = feat_in.category
					feat_out.name = feat_in.name
					feat_out.direction = feat_in.direction
				]).build.build,
			new Rule('comp2comp').in('comp_in', INSTANCE.componentInstance).filter [
				val comp_in = 'comp_in'.fetch as ComponentInstance
				(comp_in.eClass.name == 'ComponentInstance')
			].build // CHANGE: we need to check that the componentInstance is not a systemIntance to avoid overlap with 
			.out('comp_out', INSTANCE.componentInstance, [
				val comp_out = 'comp_out'.fetch as ComponentInstance
				val comp_in = 'comp_in'.fetch as ComponentInstance

				comp_out.componentInstances += (comp_in.componentInstances.fetch as List<ComponentInstance>).filterNull
				comp_out.featureInstances += (comp_in.featureInstances.fetch as List<FeatureInstance>).filterNull
				comp_out.connectionInstances +=
					(comp_in.connectionInstances.fetch as List<ConnectionInstance>).filterNull

				comp_out.category = comp_in.category
				comp_out.name = comp_in.name
			]).build.build,
			new Rule('sys2sys').in('sys_in', INSTANCE.systemInstance).build.out('sys_out', INSTANCE.systemInstance, [
				val sys_out = 'sys_out'.fetch as SystemInstance
				val sys_in = 'sys_in'.fetch as SystemInstance

				sys_out.name = sys_in.name
				sys_out.connectionInstances += (sys_in.connectionInstances.fetch as List<ConnectionInstance>).filterNull
				sys_out.featureInstances += (sys_in.featureInstances.fetch as List<FeatureInstance>).filterNull
				sys_out.componentInstances += (sys_in.componentInstances.fetch as List<ComponentInstance>).filterNull
				sys_out.category = sys_in.category
			]).build.build,
			
			new Rule('conRef2conRef').in('refin', INSTANCE.connectionReference).build.out('refout',
				INSTANCE.connectionReference, [
					val refin = 'refin'.fetch as ConnectionReference
					val refout = 'refout'.fetch as ConnectionReference

					refout.source = refin.source.fetch as ConnectionInstanceEnd
					refout.destination = refin.destination.fetch as ConnectionInstanceEnd
					refout.context = refin.context.fetch as ComponentInstance
//					refout.name = refin.name
				]).build.build
		])
	}
}
