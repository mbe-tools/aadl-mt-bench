package aadl2aadl

import yamtl.core.YAMTLModule
import org.osate.aadl2.instance.InstancePackage

abstract class AbstractTransformation extends YAMTLModule {
	
		public val INSTANCE = InstancePackage.eINSTANCE
}