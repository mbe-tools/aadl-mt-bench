/**
 */
package Tgg.util;

import Tgg.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Tgg.TggPackage
 * @generated
 */
public class TggAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TggPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TggPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TggSwitch<Adapter> modelSwitch =
		new TggSwitch<Adapter>() {
			@Override
			public Adapter caseSystemToSystem(SystemToSystem object) {
				return createSystemToSystemAdapter();
			}
			@Override
			public Adapter caseConnectionToConnection(ConnectionToConnection object) {
				return createConnectionToConnectionAdapter();
			}
			@Override
			public Adapter casePortConnectionToSharedData(PortConnectionToSharedData object) {
				return createPortConnectionToSharedDataAdapter();
			}
			@Override
			public Adapter caseReference2Reference(Reference2Reference object) {
				return createReference2ReferenceAdapter();
			}
			@Override
			public Adapter caseEnd2End(End2End object) {
				return createEnd2EndAdapter();
			}
			@Override
			public Adapter caseCompToComp(CompToComp object) {
				return createCompToCompAdapter();
			}
			@Override
			public Adapter caseFeatToFeat(FeatToFeat object) {
				return createFeatToFeatAdapter();
			}
			@Override
			public Adapter caseComponentToComponent__Marker(ComponentToComponent__Marker object) {
				return createComponentToComponent__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionInstanceToConnectionInstance__Marker(ConnectionInstanceToConnectionInstance__Marker object) {
				return createConnectionInstanceToConnectionInstance__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionRefToConnectionRefForComponentContext__Marker(ConnectionRefToConnectionRefForComponentContext__Marker object) {
				return createConnectionRefToConnectionRefForComponentContext__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionReferenceForDeviceSystemContext01__Marker(ConnectionReferenceForDeviceSystemContext01__Marker object) {
				return createConnectionReferenceForDeviceSystemContext01__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionReferenceForDeviceSystemContext10__Marker(ConnectionReferenceForDeviceSystemContext10__Marker object) {
				return createConnectionReferenceForDeviceSystemContext10__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionReferenceForDeviceSystemContext11__Marker(ConnectionReferenceForDeviceSystemContext11__Marker object) {
				return createConnectionReferenceForDeviceSystemContext11__MarkerAdapter();
			}
			@Override
			public Adapter caseFeatureToFeature__Marker(FeatureToFeature__Marker object) {
				return createFeatureToFeature__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionForDeviceDestination__Marker(PortConnectionForDeviceDestination__Marker object) {
				return createPortConnectionForDeviceDestination__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionForDeviceSource__Marker(PortConnectionForDeviceSource__Marker object) {
				return createPortConnectionForDeviceSource__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtProcess00__Marker(PortConnectionToDataAccessAtProcess00__Marker object) {
				return createPortConnectionToDataAccessAtProcess00__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtProcess01__Marker(PortConnectionToDataAccessAtProcess01__Marker object) {
				return createPortConnectionToDataAccessAtProcess01__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtProcess10__Marker(PortConnectionToDataAccessAtProcess10__Marker object) {
				return createPortConnectionToDataAccessAtProcess10__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtProcess11__Marker(PortConnectionToDataAccessAtProcess11__Marker object) {
				return createPortConnectionToDataAccessAtProcess11__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtSystem00__Marker(PortConnectionToDataAccessAtSystem00__Marker object) {
				return createPortConnectionToDataAccessAtSystem00__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtSystem01__Marker(PortConnectionToDataAccessAtSystem01__Marker object) {
				return createPortConnectionToDataAccessAtSystem01__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtSystem10__Marker(PortConnectionToDataAccessAtSystem10__Marker object) {
				return createPortConnectionToDataAccessAtSystem10__MarkerAdapter();
			}
			@Override
			public Adapter casePortConnectionToDataAccessAtSystem11__Marker(PortConnectionToDataAccessAtSystem11__Marker object) {
				return createPortConnectionToDataAccessAtSystem11__MarkerAdapter();
			}
			@Override
			public Adapter caseSystemToSystem__Marker(SystemToSystem__Marker object) {
				return createSystemToSystem__MarkerAdapter();
			}
			@Override
			public Adapter caseTGGRuleApplication(TGGRuleApplication object) {
				return createTGGRuleApplicationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Tgg.SystemToSystem <em>System To System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.SystemToSystem
	 * @generated
	 */
	public Adapter createSystemToSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionToConnection <em>Connection To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionToConnection
	 * @generated
	 */
	public Adapter createConnectionToConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToSharedData <em>Port Connection To Shared Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToSharedData
	 * @generated
	 */
	public Adapter createPortConnectionToSharedDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.Reference2Reference <em>Reference2 Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.Reference2Reference
	 * @generated
	 */
	public Adapter createReference2ReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.End2End <em>End2 End</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.End2End
	 * @generated
	 */
	public Adapter createEnd2EndAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.CompToComp <em>Comp To Comp</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.CompToComp
	 * @generated
	 */
	public Adapter createCompToCompAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.FeatToFeat <em>Feat To Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.FeatToFeat
	 * @generated
	 */
	public Adapter createFeatToFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ComponentToComponent__Marker <em>Component To Component Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ComponentToComponent__Marker
	 * @generated
	 */
	public Adapter createComponentToComponent__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker <em>Connection Instance To Connection Instance Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker
	 * @generated
	 */
	public Adapter createConnectionInstanceToConnectionInstance__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker <em>Connection Ref To Connection Ref For Component Context Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker
	 * @generated
	 */
	public Adapter createConnectionRefToConnectionRefForComponentContext__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker <em>Connection Reference For Device System Context01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker
	 * @generated
	 */
	public Adapter createConnectionReferenceForDeviceSystemContext01__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker <em>Connection Reference For Device System Context10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker
	 * @generated
	 */
	public Adapter createConnectionReferenceForDeviceSystemContext10__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker <em>Connection Reference For Device System Context11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker
	 * @generated
	 */
	public Adapter createConnectionReferenceForDeviceSystemContext11__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.FeatureToFeature__Marker <em>Feature To Feature Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.FeatureToFeature__Marker
	 * @generated
	 */
	public Adapter createFeatureToFeature__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionForDeviceDestination__Marker <em>Port Connection For Device Destination Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker
	 * @generated
	 */
	public Adapter createPortConnectionForDeviceDestination__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionForDeviceSource__Marker <em>Port Connection For Device Source Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionForDeviceSource__Marker
	 * @generated
	 */
	public Adapter createPortConnectionForDeviceSource__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker <em>Port Connection To Data Access At Process00 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtProcess00__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker <em>Port Connection To Data Access At Process01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtProcess01__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker <em>Port Connection To Data Access At Process10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtProcess10__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker <em>Port Connection To Data Access At Process11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtProcess11__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker <em>Port Connection To Data Access At System00 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtSystem00__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker <em>Port Connection To Data Access At System01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtSystem01__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker <em>Port Connection To Data Access At System10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtSystem10__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker <em>Port Connection To Data Access At System11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker
	 * @generated
	 */
	public Adapter createPortConnectionToDataAccessAtSystem11__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Tgg.SystemToSystem__Marker <em>System To System Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Tgg.SystemToSystem__Marker
	 * @generated
	 */
	public Adapter createSystemToSystem__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link runtime.TGGRuleApplication <em>TGG Rule Application</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see runtime.TGGRuleApplication
	 * @generated
	 */
	public Adapter createTGGRuleApplicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TggAdapterFactory
