/**
 */
package Tgg;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Tgg.TggPackage
 * @generated
 */
public interface TggFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TggFactory eINSTANCE = Tgg.impl.TggFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>System To System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System To System</em>'.
	 * @generated
	 */
	SystemToSystem createSystemToSystem();

	/**
	 * Returns a new object of class '<em>Connection To Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection To Connection</em>'.
	 * @generated
	 */
	ConnectionToConnection createConnectionToConnection();

	/**
	 * Returns a new object of class '<em>Port Connection To Shared Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Shared Data</em>'.
	 * @generated
	 */
	PortConnectionToSharedData createPortConnectionToSharedData();

	/**
	 * Returns a new object of class '<em>Reference2 Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference2 Reference</em>'.
	 * @generated
	 */
	Reference2Reference createReference2Reference();

	/**
	 * Returns a new object of class '<em>End2 End</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>End2 End</em>'.
	 * @generated
	 */
	End2End createEnd2End();

	/**
	 * Returns a new object of class '<em>Comp To Comp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comp To Comp</em>'.
	 * @generated
	 */
	CompToComp createCompToComp();

	/**
	 * Returns a new object of class '<em>Feat To Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feat To Feat</em>'.
	 * @generated
	 */
	FeatToFeat createFeatToFeat();

	/**
	 * Returns a new object of class '<em>Component To Component Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component To Component Marker</em>'.
	 * @generated
	 */
	ComponentToComponent__Marker createComponentToComponent__Marker();

	/**
	 * Returns a new object of class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @generated
	 */
	ConnectionInstanceToConnectionInstance__Marker createConnectionInstanceToConnectionInstance__Marker();

	/**
	 * Returns a new object of class '<em>Connection Ref To Connection Ref For Component Context Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Ref To Connection Ref For Component Context Marker</em>'.
	 * @generated
	 */
	ConnectionRefToConnectionRefForComponentContext__Marker createConnectionRefToConnectionRefForComponentContext__Marker();

	/**
	 * Returns a new object of class '<em>Connection Reference For Device System Context01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Reference For Device System Context01 Marker</em>'.
	 * @generated
	 */
	ConnectionReferenceForDeviceSystemContext01__Marker createConnectionReferenceForDeviceSystemContext01__Marker();

	/**
	 * Returns a new object of class '<em>Connection Reference For Device System Context10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Reference For Device System Context10 Marker</em>'.
	 * @generated
	 */
	ConnectionReferenceForDeviceSystemContext10__Marker createConnectionReferenceForDeviceSystemContext10__Marker();

	/**
	 * Returns a new object of class '<em>Connection Reference For Device System Context11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Reference For Device System Context11 Marker</em>'.
	 * @generated
	 */
	ConnectionReferenceForDeviceSystemContext11__Marker createConnectionReferenceForDeviceSystemContext11__Marker();

	/**
	 * Returns a new object of class '<em>Feature To Feature Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature To Feature Marker</em>'.
	 * @generated
	 */
	FeatureToFeature__Marker createFeatureToFeature__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection For Device Destination Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection For Device Destination Marker</em>'.
	 * @generated
	 */
	PortConnectionForDeviceDestination__Marker createPortConnectionForDeviceDestination__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection For Device Source Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection For Device Source Marker</em>'.
	 * @generated
	 */
	PortConnectionForDeviceSource__Marker createPortConnectionForDeviceSource__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At Process00 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At Process00 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtProcess00__Marker createPortConnectionToDataAccessAtProcess00__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At Process01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At Process01 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtProcess01__Marker createPortConnectionToDataAccessAtProcess01__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At Process10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At Process10 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtProcess10__Marker createPortConnectionToDataAccessAtProcess10__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At Process11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At Process11 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtProcess11__Marker createPortConnectionToDataAccessAtProcess11__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At System00 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At System00 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtSystem00__Marker createPortConnectionToDataAccessAtSystem00__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At System01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At System01 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtSystem01__Marker createPortConnectionToDataAccessAtSystem01__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At System10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At System10 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtSystem10__Marker createPortConnectionToDataAccessAtSystem10__Marker();

	/**
	 * Returns a new object of class '<em>Port Connection To Data Access At System11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Data Access At System11 Marker</em>'.
	 * @generated
	 */
	PortConnectionToDataAccessAtSystem11__Marker createPortConnectionToDataAccessAtSystem11__Marker();

	/**
	 * Returns a new object of class '<em>System To System Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System To System Marker</em>'.
	 * @generated
	 */
	SystemToSystem__Marker createSystemToSystem__Marker();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TggPackage getTggPackage();

} //TggFactory
