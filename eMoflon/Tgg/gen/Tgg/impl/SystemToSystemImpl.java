/**
 */
package Tgg.impl;

import Tgg.SystemToSystem;
import Tgg.TggPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System To System</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SystemToSystemImpl extends CompToCompImpl implements SystemToSystem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemToSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getSystemToSystem();
	}

} //SystemToSystemImpl
