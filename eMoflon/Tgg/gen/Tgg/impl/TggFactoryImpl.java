/**
 */
package Tgg.impl;

import Tgg.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TggFactoryImpl extends EFactoryImpl implements TggFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TggFactory init() {
		try {
			TggFactory theTggFactory = (TggFactory)EPackage.Registry.INSTANCE.getEFactory(TggPackage.eNS_URI);
			if (theTggFactory != null) {
				return theTggFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TggFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TggPackage.SYSTEM_TO_SYSTEM: return createSystemToSystem();
			case TggPackage.CONNECTION_TO_CONNECTION: return createConnectionToConnection();
			case TggPackage.PORT_CONNECTION_TO_SHARED_DATA: return createPortConnectionToSharedData();
			case TggPackage.REFERENCE2_REFERENCE: return createReference2Reference();
			case TggPackage.END2_END: return createEnd2End();
			case TggPackage.COMP_TO_COMP: return createCompToComp();
			case TggPackage.FEAT_TO_FEAT: return createFeatToFeat();
			case TggPackage.COMPONENT_TO_COMPONENT_MARKER: return createComponentToComponent__Marker();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER: return createConnectionInstanceToConnectionInstance__Marker();
			case TggPackage.CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER: return createConnectionRefToConnectionRefForComponentContext__Marker();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER: return createConnectionReferenceForDeviceSystemContext01__Marker();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER: return createConnectionReferenceForDeviceSystemContext10__Marker();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER: return createConnectionReferenceForDeviceSystemContext11__Marker();
			case TggPackage.FEATURE_TO_FEATURE_MARKER: return createFeatureToFeature__Marker();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER: return createPortConnectionForDeviceDestination__Marker();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER: return createPortConnectionForDeviceSource__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER: return createPortConnectionToDataAccessAtProcess00__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER: return createPortConnectionToDataAccessAtProcess01__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER: return createPortConnectionToDataAccessAtProcess10__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER: return createPortConnectionToDataAccessAtProcess11__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER: return createPortConnectionToDataAccessAtSystem00__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER: return createPortConnectionToDataAccessAtSystem01__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER: return createPortConnectionToDataAccessAtSystem10__Marker();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER: return createPortConnectionToDataAccessAtSystem11__Marker();
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER: return createSystemToSystem__Marker();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem createSystemToSystem() {
		SystemToSystemImpl systemToSystem = new SystemToSystemImpl();
		return systemToSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection createConnectionToConnection() {
		ConnectionToConnectionImpl connectionToConnection = new ConnectionToConnectionImpl();
		return connectionToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData createPortConnectionToSharedData() {
		PortConnectionToSharedDataImpl portConnectionToSharedData = new PortConnectionToSharedDataImpl();
		return portConnectionToSharedData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference2Reference createReference2Reference() {
		Reference2ReferenceImpl reference2Reference = new Reference2ReferenceImpl();
		return reference2Reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End createEnd2End() {
		End2EndImpl end2End = new End2EndImpl();
		return end2End;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp createCompToComp() {
		CompToCompImpl compToComp = new CompToCompImpl();
		return compToComp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat createFeatToFeat() {
		FeatToFeatImpl featToFeat = new FeatToFeatImpl();
		return featToFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentToComponent__Marker createComponentToComponent__Marker() {
		ComponentToComponent__MarkerImpl componentToComponent__Marker = new ComponentToComponent__MarkerImpl();
		return componentToComponent__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceToConnectionInstance__Marker createConnectionInstanceToConnectionInstance__Marker() {
		ConnectionInstanceToConnectionInstance__MarkerImpl connectionInstanceToConnectionInstance__Marker = new ConnectionInstanceToConnectionInstance__MarkerImpl();
		return connectionInstanceToConnectionInstance__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionRefToConnectionRefForComponentContext__Marker createConnectionRefToConnectionRefForComponentContext__Marker() {
		ConnectionRefToConnectionRefForComponentContext__MarkerImpl connectionRefToConnectionRefForComponentContext__Marker = new ConnectionRefToConnectionRefForComponentContext__MarkerImpl();
		return connectionRefToConnectionRefForComponentContext__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReferenceForDeviceSystemContext01__Marker createConnectionReferenceForDeviceSystemContext01__Marker() {
		ConnectionReferenceForDeviceSystemContext01__MarkerImpl connectionReferenceForDeviceSystemContext01__Marker = new ConnectionReferenceForDeviceSystemContext01__MarkerImpl();
		return connectionReferenceForDeviceSystemContext01__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReferenceForDeviceSystemContext10__Marker createConnectionReferenceForDeviceSystemContext10__Marker() {
		ConnectionReferenceForDeviceSystemContext10__MarkerImpl connectionReferenceForDeviceSystemContext10__Marker = new ConnectionReferenceForDeviceSystemContext10__MarkerImpl();
		return connectionReferenceForDeviceSystemContext10__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReferenceForDeviceSystemContext11__Marker createConnectionReferenceForDeviceSystemContext11__Marker() {
		ConnectionReferenceForDeviceSystemContext11__MarkerImpl connectionReferenceForDeviceSystemContext11__Marker = new ConnectionReferenceForDeviceSystemContext11__MarkerImpl();
		return connectionReferenceForDeviceSystemContext11__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureToFeature__Marker createFeatureToFeature__Marker() {
		FeatureToFeature__MarkerImpl featureToFeature__Marker = new FeatureToFeature__MarkerImpl();
		return featureToFeature__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionForDeviceDestination__Marker createPortConnectionForDeviceDestination__Marker() {
		PortConnectionForDeviceDestination__MarkerImpl portConnectionForDeviceDestination__Marker = new PortConnectionForDeviceDestination__MarkerImpl();
		return portConnectionForDeviceDestination__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionForDeviceSource__Marker createPortConnectionForDeviceSource__Marker() {
		PortConnectionForDeviceSource__MarkerImpl portConnectionForDeviceSource__Marker = new PortConnectionForDeviceSource__MarkerImpl();
		return portConnectionForDeviceSource__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtProcess00__Marker createPortConnectionToDataAccessAtProcess00__Marker() {
		PortConnectionToDataAccessAtProcess00__MarkerImpl portConnectionToDataAccessAtProcess00__Marker = new PortConnectionToDataAccessAtProcess00__MarkerImpl();
		return portConnectionToDataAccessAtProcess00__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtProcess01__Marker createPortConnectionToDataAccessAtProcess01__Marker() {
		PortConnectionToDataAccessAtProcess01__MarkerImpl portConnectionToDataAccessAtProcess01__Marker = new PortConnectionToDataAccessAtProcess01__MarkerImpl();
		return portConnectionToDataAccessAtProcess01__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtProcess10__Marker createPortConnectionToDataAccessAtProcess10__Marker() {
		PortConnectionToDataAccessAtProcess10__MarkerImpl portConnectionToDataAccessAtProcess10__Marker = new PortConnectionToDataAccessAtProcess10__MarkerImpl();
		return portConnectionToDataAccessAtProcess10__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtProcess11__Marker createPortConnectionToDataAccessAtProcess11__Marker() {
		PortConnectionToDataAccessAtProcess11__MarkerImpl portConnectionToDataAccessAtProcess11__Marker = new PortConnectionToDataAccessAtProcess11__MarkerImpl();
		return portConnectionToDataAccessAtProcess11__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtSystem00__Marker createPortConnectionToDataAccessAtSystem00__Marker() {
		PortConnectionToDataAccessAtSystem00__MarkerImpl portConnectionToDataAccessAtSystem00__Marker = new PortConnectionToDataAccessAtSystem00__MarkerImpl();
		return portConnectionToDataAccessAtSystem00__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtSystem01__Marker createPortConnectionToDataAccessAtSystem01__Marker() {
		PortConnectionToDataAccessAtSystem01__MarkerImpl portConnectionToDataAccessAtSystem01__Marker = new PortConnectionToDataAccessAtSystem01__MarkerImpl();
		return portConnectionToDataAccessAtSystem01__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtSystem10__Marker createPortConnectionToDataAccessAtSystem10__Marker() {
		PortConnectionToDataAccessAtSystem10__MarkerImpl portConnectionToDataAccessAtSystem10__Marker = new PortConnectionToDataAccessAtSystem10__MarkerImpl();
		return portConnectionToDataAccessAtSystem10__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToDataAccessAtSystem11__Marker createPortConnectionToDataAccessAtSystem11__Marker() {
		PortConnectionToDataAccessAtSystem11__MarkerImpl portConnectionToDataAccessAtSystem11__Marker = new PortConnectionToDataAccessAtSystem11__MarkerImpl();
		return portConnectionToDataAccessAtSystem11__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem__Marker createSystemToSystem__Marker() {
		SystemToSystem__MarkerImpl systemToSystem__Marker = new SystemToSystem__MarkerImpl();
		return systemToSystem__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggPackage getTggPackage() {
		return (TggPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TggPackage getPackage() {
		return TggPackage.eINSTANCE;
	}

} //TggFactoryImpl
