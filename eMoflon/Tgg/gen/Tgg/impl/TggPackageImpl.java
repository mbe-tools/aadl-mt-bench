/**
 */
package Tgg.impl;

import Tgg.TggFactory;
import Tgg.TggPackage;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.osate.aadl2.Aadl2Package;

import org.osate.aadl2.instance.InstancePackage;

import runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TggPackageImpl extends EPackageImpl implements TggPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "Tgg.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemToSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionToConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToSharedDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reference2ReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass end2EndEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compToCompEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featToFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentToComponent__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionInstanceToConnectionInstance__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionRefToConnectionRefForComponentContext__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionReferenceForDeviceSystemContext01__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionReferenceForDeviceSystemContext10__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionReferenceForDeviceSystemContext11__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureToFeature__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionForDeviceDestination__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionForDeviceSource__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtProcess00__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtProcess01__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtProcess10__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtProcess11__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtSystem00__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtSystem01__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtSystem10__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToDataAccessAtSystem11__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemToSystem__MarkerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Tgg.TggPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TggPackageImpl() {
		super(eNS_URI, TggFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link TggPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static TggPackage init() {
		if (isInited) return (TggPackage)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredTggPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		TggPackageImpl theTggPackage = registeredTggPackage instanceof TggPackageImpl ? (TggPackageImpl)registeredTggPackage : new TggPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		InstancePackage.eINSTANCE.eClass();
		RuntimePackage.eINSTANCE.eClass();
		Aadl2Package.eINSTANCE.eClass();

		// Load packages
		theTggPackage.loadPackage();

		// Fix loaded packages
		theTggPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theTggPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TggPackage.eNS_URI, theTggPackage);
		return theTggPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemToSystem() {
		if (systemToSystemEClass == null) {
			systemToSystemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(0);
		}
		return systemToSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionToConnection() {
		if (connectionToConnectionEClass == null) {
			connectionToConnectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(1);
		}
		return connectionToConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionToConnection_Source() {
        return (EReference)getConnectionToConnection().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionToConnection_Target() {
        return (EReference)getConnectionToConnection().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToSharedData() {
		if (portConnectionToSharedDataEClass == null) {
			portConnectionToSharedDataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(2);
		}
		return portConnectionToSharedDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToSharedData_Source() {
        return (EReference)getPortConnectionToSharedData().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToSharedData_Target() {
        return (EReference)getPortConnectionToSharedData().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReference2Reference() {
		if (reference2ReferenceEClass == null) {
			reference2ReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(3);
		}
		return reference2ReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference2Reference_Source() {
        return (EReference)getReference2Reference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference2Reference_Target() {
        return (EReference)getReference2Reference().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnd2End() {
		if (end2EndEClass == null) {
			end2EndEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(4);
		}
		return end2EndEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnd2End_Source() {
        return (EReference)getEnd2End().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnd2End_Target() {
        return (EReference)getEnd2End().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompToComp() {
		if (compToCompEClass == null) {
			compToCompEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(5);
		}
		return compToCompEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatToFeat() {
		if (featToFeatEClass == null) {
			featToFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(6);
		}
		return featToFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentToComponent__Marker() {
		if (componentToComponent__MarkerEClass == null) {
			componentToComponent__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(7);
		}
		return componentToComponent__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__SRC__subcomponent_source() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__TRG__subcomponent_target() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__CORR__sub2sub() {
        return (EReference)getComponentToComponent__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionInstanceToConnectionInstance__Marker() {
		if (connectionInstanceToConnectionInstance__MarkerEClass == null) {
			connectionInstanceToConnectionInstance__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(8);
		}
		return connectionInstanceToConnectionInstance__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2() {
        return (EReference)getConnectionInstanceToConnectionInstance__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionRefToConnectionRefForComponentContext__Marker() {
		if (connectionRefToConnectionRefForComponentContext__MarkerEClass == null) {
			connectionRefToConnectionRefForComponentContext__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(9);
		}
		return connectionRefToConnectionRefForComponentContext__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__SRC__connectionRef_source() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__connection_source() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_context() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_destination() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_source() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__TRG__connectionRef_target() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__connection_target() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_context() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_destination() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_source() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__comp2comp() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__e2e1() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__e2e2() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__CORR__r2r() {
        return (EReference)getConnectionRefToConnectionRefForComponentContext__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionReferenceForDeviceSystemContext01__Marker() {
		if (connectionReferenceForDeviceSystemContext01__MarkerEClass == null) {
			connectionReferenceForDeviceSystemContext01__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(10);
		}
		return connectionReferenceForDeviceSystemContext01__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__SRC__connectionRef_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__connection_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__s_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__s_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__SRC__s_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__TRG__connectionRef_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__connection_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__t_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__t_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__TRG__t_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__comp2comp() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__e2e1() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__CORR__e2e2() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__CORR__r2r() {
        return (EReference)getConnectionReferenceForDeviceSystemContext01__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionReferenceForDeviceSystemContext10__Marker() {
		if (connectionReferenceForDeviceSystemContext10__MarkerEClass == null) {
			connectionReferenceForDeviceSystemContext10__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(11);
		}
		return connectionReferenceForDeviceSystemContext10__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__connectionRef_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__connection_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__s_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__connectionRef_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__connection_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__t_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__comp2comp() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__e2e1() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__e2e2() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__r2r() {
        return (EReference)getConnectionReferenceForDeviceSystemContext10__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionReferenceForDeviceSystemContext11__Marker() {
		if (connectionReferenceForDeviceSystemContext11__MarkerEClass == null) {
			connectionReferenceForDeviceSystemContext11__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(12);
		}
		return connectionReferenceForDeviceSystemContext11__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__SRC__connectionRef_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__connection_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__TRG__connectionRef_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__connection_target() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_context() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_destination() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_source() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__comp2comp() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__e2e1() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__e2e2() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__CORR__r2r() {
        return (EReference)getConnectionReferenceForDeviceSystemContext11__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureToFeature__Marker() {
		if (featureToFeature__MarkerEClass == null) {
			featureToFeature__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(13);
		}
		return featureToFeature__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__SRC__feature_source() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__TRG__feature_target() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__CORR__f2f2() {
        return (EReference)getFeatureToFeature__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionForDeviceDestination__Marker() {
		if (portConnectionForDeviceDestination__MarkerEClass == null) {
			portConnectionForDeviceDestination__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(14);
		}
		return portConnectionForDeviceDestination__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__component_destination() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__component_destination_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__component_source_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__connection_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__feature_destination_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__feature_source_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__c2c1() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__c2c2() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__co2co() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__feature2feature1() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__feature2feature2() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionForDeviceDestination__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionForDeviceSource__Marker() {
		if (portConnectionForDeviceSource__MarkerEClass == null) {
			portConnectionForDeviceSource__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(15);
		}
		return portConnectionForDeviceSource__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_destination() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_destination_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_source_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__connection_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_destination_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_source_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c1() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c2() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__co2co() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature1() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature2() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionForDeviceSource__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtProcess00__Marker() {
		if (portConnectionToDataAccessAtProcess00__MarkerEClass == null) {
			portConnectionToDataAccessAtProcess00__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(16);
		}
		return portConnectionToDataAccessAtProcess00__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_source_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__data() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2d() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub1() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub2() {
        return (EReference)getPortConnectionToDataAccessAtProcess00__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtProcess01__Marker() {
		if (portConnectionToDataAccessAtProcess01__MarkerEClass == null) {
			portConnectionToDataAccessAtProcess01__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(17);
		}
		return portConnectionToDataAccessAtProcess01__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_conn_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_conn_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_conn_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_conn_source_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__data() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__dataaccess1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__dataaccess2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__feature1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__feature2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__refconnection1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__refconnection2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__f2f1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__f2f2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2d() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2r1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2r2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__sub2sub1() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__sub2sub2() {
        return (EReference)getPortConnectionToDataAccessAtProcess01__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtProcess10__Marker() {
		if (portConnectionToDataAccessAtProcess10__MarkerEClass == null) {
			portConnectionToDataAccessAtProcess10__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(18);
		}
		return portConnectionToDataAccessAtProcess10__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_conn_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_conn_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_conn_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_conn_source_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__data() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__dataaccess1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__dataaccess2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__feature1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__feature2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__refconnection1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__refconnection2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__f2f1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__f2f2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2d() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2r1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2r2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__sub2sub1() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__sub2sub2() {
        return (EReference)getPortConnectionToDataAccessAtProcess10__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtProcess11__Marker() {
		if (portConnectionToDataAccessAtProcess11__MarkerEClass == null) {
			portConnectionToDataAccessAtProcess11__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(19);
		}
		return portConnectionToDataAccessAtProcess11__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_conn_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_conn_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_conn_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_conn_source_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_target() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__data() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__dataaccess1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__dataaccess2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__feature1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__feature2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__refconnection1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__refconnection2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__c2c() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__f2f1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__f2f2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2d() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2r1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2r2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__sub2sub1() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__sub2sub2() {
        return (EReference)getPortConnectionToDataAccessAtProcess11__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtSystem00__Marker() {
		if (portConnectionToDataAccessAtSystem00__MarkerEClass == null) {
			portConnectionToDataAccessAtSystem00__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(20);
		}
		return portConnectionToDataAccessAtSystem00__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__process_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__process_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__ref_connection_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__thread_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__thread_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__data_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__data_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__dataaccess_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__dataaccess_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__feature_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__feature_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__process_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__process_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__ref_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__ref_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__thread_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__thread_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__feature2feature1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__feature2feature2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2d1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2d2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__process2process1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__process2process2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__r2r1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__r2r2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__thread2thread1() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__thread2thread2() {
        return (EReference)getPortConnectionToDataAccessAtSystem00__Marker().getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtSystem01__Marker() {
		if (portConnectionToDataAccessAtSystem01__MarkerEClass == null) {
			portConnectionToDataAccessAtSystem01__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(21);
		}
		return portConnectionToDataAccessAtSystem01__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__process_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__process_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__ref_connection_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__thread_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__thread_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__data_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__data_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__dataaccess_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__dataaccess_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__feature_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__feature_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__process_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__process_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__ref_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__ref_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__thread_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__thread_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__feature2feature1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__feature2feature2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2d1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2d2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__process2process1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__process2process2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__r2r1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__r2r2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__thread2thread1() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__thread2thread2() {
        return (EReference)getPortConnectionToDataAccessAtSystem01__Marker().getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtSystem10__Marker() {
		if (portConnectionToDataAccessAtSystem10__MarkerEClass == null) {
			portConnectionToDataAccessAtSystem10__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(22);
		}
		return portConnectionToDataAccessAtSystem10__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__process_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__process_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__ref_connection_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__thread_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__thread_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__data_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__data_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__dataaccess_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__dataaccess_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__feature_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__feature_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__process_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__process_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__ref_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__ref_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__thread_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__thread_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__feature2feature1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__feature2feature2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2d1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2d2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__process2process1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__process2process2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__r2r1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__r2r2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__thread2thread1() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__thread2thread2() {
        return (EReference)getPortConnectionToDataAccessAtSystem10__Marker().getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToDataAccessAtSystem11__Marker() {
		if (portConnectionToDataAccessAtSystem11__MarkerEClass == null) {
			portConnectionToDataAccessAtSystem11__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(23);
		}
		return portConnectionToDataAccessAtSystem11__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__system_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_destination() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_source() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__system_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_destination_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_source_target() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__s2s() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread1() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread2() {
        return (EReference)getPortConnectionToDataAccessAtSystem11__Marker().getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemToSystem__Marker() {
		if (systemToSystem__MarkerEClass == null) {
			systemToSystem__MarkerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TggPackage.eNS_URI).getEClassifiers().get(24);
		}
		return systemToSystem__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__SRC__system_source() {
        return (EReference)getSystemToSystem__Marker().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__TRG__system_target() {
        return (EReference)getSystemToSystem__Marker().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__CORR__s2s() {
        return (EReference)getSystemToSystem__Marker().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggFactory getTggFactory() {
		return (TggFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("Tgg." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //TggPackageImpl
