package org.emoflon.ibex.tgg.operational.csp.constraints.factories.copying;

import java.util.HashMap;
import java.util.HashSet;			

import org.emoflon.ibex.tgg.operational.csp.constraints.factories.RuntimeTGGAttrConstraintFactory;			

import org.emoflon.ibex.tgg.operational.csp.constraints.custom.copying.UserDefined_eq_enum;

public class UserDefinedRuntimeTGGAttrConstraintFactory extends RuntimeTGGAttrConstraintFactory {

	public UserDefinedRuntimeTGGAttrConstraintFactory() {
		super();
	}
	
	@Override
	protected void initialize() {
		creators = new HashMap<>();
		creators.put("eq_enum", () -> new UserDefined_eq_enum());

		constraints = new HashSet<String>();
		constraints.addAll(creators.keySet());
	}
}
