package org.emoflon.ibex.tgg.run.copying.config;

import java.io.IOException;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.emoflon.ibex.tgg.operational.csp.constraints.factories.copying.UserDefinedRuntimeTGGAttrConstraintFactory;
import org.emoflon.ibex.tgg.operational.defaults.IbexOptions;
import org.emoflon.ibex.tgg.compiler.defaults.IRegistrationHelper;
import org.emoflon.ibex.tgg.compiler.patterns.ACStrategy;
import org.emoflon.ibex.tgg.operational.strategies.modules.IbexExecutable;
import org.emoflon.ibex.tgg.runtime.democles.DemoclesTGGEngine;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.instance.InstancePackage;

import Copying.CopyingPackage;

public class DemoclesRegistrationHelper implements IRegistrationHelper {

	/** Load and register source and target metamodels */
	public void registerMetamodels(ResourceSet rs, IbexExecutable executable) throws IOException {

		rs.getPackageRegistry().put("platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg/model/Tgg.ecore",
				CopyingPackage.eINSTANCE);

		EPackage.Registry.INSTANCE.put(org.osate.aadl2.Aadl2Package.eNS_URI, 
				Aadl2Package.eINSTANCE);
		EPackage.Registry.INSTANCE.put(org.osate.aadl2.instance.InstancePackage.eNS_URI,
				InstancePackage.eINSTANCE);

	}

	/** Create default options **/
	public IbexOptions createIbexOptions() {
		IbexOptions options = new IbexOptions();
		options.blackInterpreter(new DemoclesTGGEngine());
		options.project.name("Copying");
		options.project.path("Copying");
		options.debug.ibexDebug(false);
		options.csp.userDefinedConstraints(new UserDefinedRuntimeTGGAttrConstraintFactory());
		options.registrationHelper(this);
		options.patterns.acStrategy(ACStrategy.NONE);
		return options;
	}
}
