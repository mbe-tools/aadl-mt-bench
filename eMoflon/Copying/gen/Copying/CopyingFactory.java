/**
 */
package Copying;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Copying.CopyingPackage
 * @generated
 */
public interface CopyingFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CopyingFactory eINSTANCE = Copying.impl.CopyingFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>System To System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System To System</em>'.
	 * @generated
	 */
	SystemToSystem createSystemToSystem();

	/**
	 * Returns a new object of class '<em>Connection To Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection To Connection</em>'.
	 * @generated
	 */
	ConnectionToConnection createConnectionToConnection();

	/**
	 * Returns a new object of class '<em>Port Connection To Shared Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection To Shared Data</em>'.
	 * @generated
	 */
	PortConnectionToSharedData createPortConnectionToSharedData();

	/**
	 * Returns a new object of class '<em>Reference2 Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference2 Reference</em>'.
	 * @generated
	 */
	Reference2Reference createReference2Reference();

	/**
	 * Returns a new object of class '<em>End2 End</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>End2 End</em>'.
	 * @generated
	 */
	End2End createEnd2End();

	/**
	 * Returns a new object of class '<em>Comp To Comp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comp To Comp</em>'.
	 * @generated
	 */
	CompToComp createCompToComp();

	/**
	 * Returns a new object of class '<em>Feat To Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feat To Feat</em>'.
	 * @generated
	 */
	FeatToFeat createFeatToFeat();

	/**
	 * Returns a new object of class '<em>Component To Component Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component To Component Marker</em>'.
	 * @generated
	 */
	ComponentToComponent__Marker createComponentToComponent__Marker();

	/**
	 * Returns a new object of class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @generated
	 */
	ConnectionInstanceToConnectionInstance__Marker createConnectionInstanceToConnectionInstance__Marker();

	/**
	 * Returns a new object of class '<em>Connection Ref To Connection Ref Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Ref To Connection Ref Marker</em>'.
	 * @generated
	 */
	ConnectionRefToConnectionRef__Marker createConnectionRefToConnectionRef__Marker();

	/**
	 * Returns a new object of class '<em>Feature To Feature Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature To Feature Marker</em>'.
	 * @generated
	 */
	FeatureToFeature__Marker createFeatureToFeature__Marker();

	/**
	 * Returns a new object of class '<em>System To System Marker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System To System Marker</em>'.
	 * @generated
	 */
	SystemToSystem__Marker createSystemToSystem__Marker();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CopyingPackage getCopyingPackage();

} //CopyingFactory
