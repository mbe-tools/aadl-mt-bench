/**
 */
package Copying.impl;

import Copying.CompToComp;
import Copying.CopyingPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comp To Comp</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CompToCompImpl extends End2EndImpl implements CompToComp {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompToCompImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.COMP_TO_COMP;
	}

} //CompToCompImpl
