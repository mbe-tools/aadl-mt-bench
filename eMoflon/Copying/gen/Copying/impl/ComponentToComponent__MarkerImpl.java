/**
 */
package Copying.impl;

import Copying.CompToComp;
import Copying.ComponentToComponent__Marker;
import Copying.CopyingPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component To Component Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCREATE__SRC__subcomponent_source <em>CREATE SRC subcomponent source</em>}</li>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCREATE__TRG__subcomponent_target <em>CREATE TRG subcomponent target</em>}</li>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Copying.impl.ComponentToComponent__MarkerImpl#getCREATE__CORR__sub2sub <em>CREATE CORR sub2sub</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentToComponent__MarkerImpl extends TGGRuleApplicationImpl implements ComponentToComponent__Marker {
	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_source() <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__subcomponent_source() <em>CREATE SRC subcomponent source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__subcomponent_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance creatE__SRC__subcomponent_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_target() <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__subcomponent_target() <em>CREATE TRG subcomponent target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__subcomponent_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance creatE__TRG__subcomponent_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c() <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__c2c;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__sub2sub() <em>CREATE CORR sub2sub</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__sub2sub()
	 * @generated
	 * @ordered
	 */
	protected CompToComp creatE__CORR__sub2sub;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentToComponent__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.COMPONENT_TO_COMPONENT_MARKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_source() {
		if (contexT__SRC__component_source != null && contexT__SRC__component_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_source = (InternalEObject)contexT__SRC__component_source;
			contexT__SRC__component_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_source);
			if (contexT__SRC__component_source != oldCONTEXT__SRC__component_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
			}
		}
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_source() {
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_source(ComponentInstance newCONTEXT__SRC__component_source) {
		ComponentInstance oldCONTEXT__SRC__component_source = contexT__SRC__component_source;
		contexT__SRC__component_source = newCONTEXT__SRC__component_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCREATE__SRC__subcomponent_source() {
		if (creatE__SRC__subcomponent_source != null && creatE__SRC__subcomponent_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__subcomponent_source = (InternalEObject)creatE__SRC__subcomponent_source;
			creatE__SRC__subcomponent_source = (ComponentInstance)eResolveProxy(oldCREATE__SRC__subcomponent_source);
			if (creatE__SRC__subcomponent_source != oldCREATE__SRC__subcomponent_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE, oldCREATE__SRC__subcomponent_source, creatE__SRC__subcomponent_source));
			}
		}
		return creatE__SRC__subcomponent_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCREATE__SRC__subcomponent_source() {
		return creatE__SRC__subcomponent_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__subcomponent_source(ComponentInstance newCREATE__SRC__subcomponent_source) {
		ComponentInstance oldCREATE__SRC__subcomponent_source = creatE__SRC__subcomponent_source;
		creatE__SRC__subcomponent_source = newCREATE__SRC__subcomponent_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE, oldCREATE__SRC__subcomponent_source, creatE__SRC__subcomponent_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_target() {
		if (contexT__TRG__component_target != null && contexT__TRG__component_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_target = (InternalEObject)contexT__TRG__component_target;
			contexT__TRG__component_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_target);
			if (contexT__TRG__component_target != oldCONTEXT__TRG__component_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
			}
		}
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_target() {
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_target(ComponentInstance newCONTEXT__TRG__component_target) {
		ComponentInstance oldCONTEXT__TRG__component_target = contexT__TRG__component_target;
		contexT__TRG__component_target = newCONTEXT__TRG__component_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCREATE__TRG__subcomponent_target() {
		if (creatE__TRG__subcomponent_target != null && creatE__TRG__subcomponent_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__subcomponent_target = (InternalEObject)creatE__TRG__subcomponent_target;
			creatE__TRG__subcomponent_target = (ComponentInstance)eResolveProxy(oldCREATE__TRG__subcomponent_target);
			if (creatE__TRG__subcomponent_target != oldCREATE__TRG__subcomponent_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET, oldCREATE__TRG__subcomponent_target, creatE__TRG__subcomponent_target));
			}
		}
		return creatE__TRG__subcomponent_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCREATE__TRG__subcomponent_target() {
		return creatE__TRG__subcomponent_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__subcomponent_target(ComponentInstance newCREATE__TRG__subcomponent_target) {
		ComponentInstance oldCREATE__TRG__subcomponent_target = creatE__TRG__subcomponent_target;
		creatE__TRG__subcomponent_target = newCREATE__TRG__subcomponent_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET, oldCREATE__TRG__subcomponent_target, creatE__TRG__subcomponent_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__c2c() {
		if (contexT__CORR__c2c != null && contexT__CORR__c2c.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c = (InternalEObject)contexT__CORR__c2c;
			contexT__CORR__c2c = (CompToComp)eResolveProxy(oldCONTEXT__CORR__c2c);
			if (contexT__CORR__c2c != oldCONTEXT__CORR__c2c) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
			}
		}
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__c2c() {
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c(CompToComp newCONTEXT__CORR__c2c) {
		CompToComp oldCONTEXT__CORR__c2c = contexT__CORR__c2c;
		contexT__CORR__c2c = newCONTEXT__CORR__c2c;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCREATE__CORR__sub2sub() {
		if (creatE__CORR__sub2sub != null && creatE__CORR__sub2sub.eIsProxy()) {
			InternalEObject oldCREATE__CORR__sub2sub = (InternalEObject)creatE__CORR__sub2sub;
			creatE__CORR__sub2sub = (CompToComp)eResolveProxy(oldCREATE__CORR__sub2sub);
			if (creatE__CORR__sub2sub != oldCREATE__CORR__sub2sub) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB, oldCREATE__CORR__sub2sub, creatE__CORR__sub2sub));
			}
		}
		return creatE__CORR__sub2sub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCREATE__CORR__sub2sub() {
		return creatE__CORR__sub2sub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__sub2sub(CompToComp newCREATE__CORR__sub2sub) {
		CompToComp oldCREATE__CORR__sub2sub = creatE__CORR__sub2sub;
		creatE__CORR__sub2sub = newCREATE__CORR__sub2sub;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB, oldCREATE__CORR__sub2sub, creatE__CORR__sub2sub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_source();
				return basicGetCONTEXT__SRC__component_source();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE:
				if (resolve) return getCREATE__SRC__subcomponent_source();
				return basicGetCREATE__SRC__subcomponent_source();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				if (resolve) return getCONTEXT__TRG__component_target();
				return basicGetCONTEXT__TRG__component_target();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET:
				if (resolve) return getCREATE__TRG__subcomponent_target();
				return basicGetCREATE__TRG__subcomponent_target();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C:
				if (resolve) return getCONTEXT__CORR__c2c();
				return basicGetCONTEXT__CORR__c2c();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB:
				if (resolve) return getCREATE__CORR__sub2sub();
				return basicGetCREATE__CORR__sub2sub();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)newValue);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE:
				setCREATE__SRC__subcomponent_source((ComponentInstance)newValue);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)newValue);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET:
				setCREATE__TRG__subcomponent_target((ComponentInstance)newValue);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)newValue);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB:
				setCREATE__CORR__sub2sub((CompToComp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)null);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE:
				setCREATE__SRC__subcomponent_source((ComponentInstance)null);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)null);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET:
				setCREATE__TRG__subcomponent_target((ComponentInstance)null);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)null);
				return;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB:
				setCREATE__CORR__sub2sub((CompToComp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				return contexT__SRC__component_source != null;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE:
				return creatE__SRC__subcomponent_source != null;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				return contexT__TRG__component_target != null;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET:
				return creatE__TRG__subcomponent_target != null;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C:
				return contexT__CORR__c2c != null;
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB:
				return creatE__CORR__sub2sub != null;
		}
		return super.eIsSet(featureID);
	}

} //ComponentToComponent__MarkerImpl
