/**
 */
package Copying;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comp To Comp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Copying.CopyingPackage#getCompToComp()
 * @model
 * @generated
 */
public interface CompToComp extends End2End {
} // CompToComp
