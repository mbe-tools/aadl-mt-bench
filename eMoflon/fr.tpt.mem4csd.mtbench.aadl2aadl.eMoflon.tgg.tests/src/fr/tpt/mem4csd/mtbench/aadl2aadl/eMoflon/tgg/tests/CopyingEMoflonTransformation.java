package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import org.emoflon.ibex.tgg.run.copying.config.DemoclesRegistrationHelper;

public class CopyingEMoflonTransformation extends AbstractEMoflonTransformation {

	public CopyingEMoflonTransformation(	final String inputModelName, 
											final String outputModelSuffix )
	throws IOException {
		super( inputModelName, outputModelSuffix, new DemoclesRegistrationHelper() );
	}
}