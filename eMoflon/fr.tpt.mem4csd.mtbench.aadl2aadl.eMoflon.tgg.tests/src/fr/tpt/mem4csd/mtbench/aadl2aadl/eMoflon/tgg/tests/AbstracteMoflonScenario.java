package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ModelModif;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.Result;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public abstract class AbstracteMoflonScenario extends AbstractScenario {

	protected AbstracteMoflonScenario() {
		super();
	}

	@Override
	public Resource executeScenario(final int testNumber, final Result res, final String inputModelName)
			throws Exception {
		ModelModif modif = new ModelModif();
		long[] runs = new long[res.getRuntimes()];
		long t0, t1;
		System.out.println("Test " + testNumber + " " + inputModelName + " Performing transformation:");

		t0 = System.nanoTime();
		AbstractEMoflonTransformation transformation = getSpecification(inputModelName);
		t1 = System.nanoTime();
		System.out.println("Init " + testNumber + " in " + Long.toString(t1 - t0) + " ns");

		runs = scenarioSteps(getScenario(), transformation.getResourceHandler().getSourceResource(), modif,
				res.getRuntimes(), transformation);

		res.setExecutionTimes(inputModelName, testNumber, res.getMedian(runs));
		System.out.println("Test " + testNumber + " in " + res.getMedian(runs) + " ns");

		transformation.saveModels();
		transformation.terminate();
		System.out.println("Transformation finished.");

		return transformation.getResourceHandler().getTargetResource();
	}

	abstract AbstractEMoflonTransformation getSpecification(String inputModelName) throws IOException;

	abstract ScenarioKind getScenario();

	protected long[] scenarioSteps(ScenarioKind scenarioKind, Resource leftSystem, ModelModif modif, int runtimes,
			AbstractEMoflonTransformation eMoflonTransformation) throws IOException {
		long t0, t1;
		long[] runs = new long[runtimes];

		if (scenarioKind != ScenarioKind.BATCH) {
			eMoflonTransformation.forward();
		}
		for (int i = 0; i < runtimes; i++) {
			modif.applyModif(scenarioKind, leftSystem);
			t0 = System.nanoTime();
			eMoflonTransformation.forward();
			t1 = System.nanoTime();
			runs[i] = t1 - t0;
		}

		return runs;
	}

}
