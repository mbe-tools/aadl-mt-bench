package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingAddition extends AbstracteMoflonScenario {

	public CopyingAddition() {
		super();
	}

	@Override
	AbstractEMoflonTransformation getSpecification(String inputModelName) throws IOException {
		return new CopyingEMoflonTransformation(inputModelName, BenchmarkConstants.COPY_ADD_SUFFIX);
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}
}
