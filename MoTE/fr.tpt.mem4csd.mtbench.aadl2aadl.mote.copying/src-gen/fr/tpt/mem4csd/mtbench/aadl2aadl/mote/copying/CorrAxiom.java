/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage#getCorrAxiom()
 * @model
 * @generated
 */
public interface CorrAxiom extends CorrComponent2Component {
} // CorrAxiom
