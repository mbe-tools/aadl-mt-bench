/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated;

import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>copying Operational TGG</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage#getcopyingOperationalTGG()
 * @model
 * @generated
 */
public interface copyingOperationalTGG extends SdmOperationalTGG {
} // copyingOperationalTGG
