/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage
 * @generated
 */
public interface GeneratedFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneratedFactory eINSTANCE = fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>copying Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>copying Operational TGG</em>'.
	 * @generated
	 */
	copyingOperationalTGG createcopyingOperationalTGG();

	/**
	 * Returns a new object of class '<em>mote Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>mote Axiom</em>'.
	 * @generated
	 */
	moteAxiom createmoteAxiom();

	/**
	 * Returns a new object of class '<em>connection2connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connection2connection</em>'.
	 * @generated
	 */
	connection2connection createconnection2connection();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref</em>'.
	 * @generated
	 */
	connectionref2connectionref createconnectionref2connectionref();

	/**
	 * Returns a new object of class '<em>feature2feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>feature2feature</em>'.
	 * @generated
	 */
	feature2feature createfeature2feature();

	/**
	 * Returns a new object of class '<em>subcomponent2subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>subcomponent2subcomponent</em>'.
	 * @generated
	 */
	subcomponent2subcomponent createsubcomponent2subcomponent();

	/**
	 * Returns a new object of class '<em>mote Axiom r0</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>mote Axiom r0</em>'.
	 * @generated
	 */
	moteAxiom_r0 createmoteAxiom_r0();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref r60ff</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref r60ff</em>'.
	 * @generated
	 */
	connectionref2connectionref_r60ff createconnectionref2connectionref_r60ff();

	/**
	 * Returns a new object of class '<em>feature2feature r3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>feature2feature r3</em>'.
	 * @generated
	 */
	feature2feature_r3 createfeature2feature_r3();

	/**
	 * Returns a new object of class '<em>subcomponent2subcomponent r2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>subcomponent2subcomponent r2</em>'.
	 * @generated
	 */
	subcomponent2subcomponent_r2 createsubcomponent2subcomponent_r2();

	/**
	 * Returns a new object of class '<em>connection2connection r90p</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connection2connection r90p</em>'.
	 * @generated
	 */
	connection2connection_r90p createconnection2connection_r90p();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GeneratedPackage getGeneratedPackage();

} //GeneratedFactory
