/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGGPackage;

import de.mdelab.mltgg.mote2.sdm.SdmPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedFactory
 * @model kind="package"
 * @generated
 */
public interface GeneratedPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "generated";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/mtbench.aadl2aadl/copying/generated";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "copying.generated";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneratedPackage eINSTANCE = fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.copyingOperationalTGGImpl <em>copying Operational TGG</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.copyingOperationalTGGImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getcopyingOperationalTGG()
	 * @generated
	 */
	int COPYING_OPERATIONAL_TGG = 0;

	/**
	 * The feature id for the '<em><b>Operational Axiom Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__OPERATIONAL_AXIOM_GROUP = SdmPackage.SDM_OPERATIONAL_TGG__OPERATIONAL_AXIOM_GROUP;

	/**
	 * The feature id for the '<em><b>Operational Rule Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__OPERATIONAL_RULE_GROUPS = SdmPackage.SDM_OPERATIONAL_TGG__OPERATIONAL_RULE_GROUPS;

	/**
	 * The feature id for the '<em><b>Tgg Engine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__TGG_ENGINE = SdmPackage.SDM_OPERATIONAL_TGG__TGG_ENGINE;

	/**
	 * The feature id for the '<em><b>Tgg ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__TGG_ID = SdmPackage.SDM_OPERATIONAL_TGG__TGG_ID;

	/**
	 * The feature id for the '<em><b>Unidirectional References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__UNIDIRECTIONAL_REFERENCES = SdmPackage.SDM_OPERATIONAL_TGG__UNIDIRECTIONAL_REFERENCES;

	/**
	 * The feature id for the '<em><b>Interpreter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG__INTERPRETER = SdmPackage.SDM_OPERATIONAL_TGG__INTERPRETER;

	/**
	 * The number of structural features of the '<em>copying Operational TGG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG_FEATURE_COUNT = SdmPackage.SDM_OPERATIONAL_TGG_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Transformation Started</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG___TRANSFORMATION_STARTED = SdmPackage.SDM_OPERATIONAL_TGG___TRANSFORMATION_STARTED;

	/**
	 * The operation id for the '<em>Transformation Finished</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG___TRANSFORMATION_FINISHED = SdmPackage.SDM_OPERATIONAL_TGG___TRANSFORMATION_FINISHED;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG___INIT = SdmPackage.SDM_OPERATIONAL_TGG___INIT;

	/**
	 * The operation id for the '<em>Get Operational Mapping Group</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG___GET_OPERATIONAL_MAPPING_GROUP__STRING = SdmPackage.SDM_OPERATIONAL_TGG___GET_OPERATIONAL_MAPPING_GROUP__STRING;

	/**
	 * The operation id for the '<em>Execute Activity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG___EXECUTE_ACTIVITY__ACTIVITY_EMAP = SdmPackage.SDM_OPERATIONAL_TGG___EXECUTE_ACTIVITY__ACTIVITY_EMAP;

	/**
	 * The number of operations of the '<em>copying Operational TGG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPYING_OPERATIONAL_TGG_OPERATION_COUNT = SdmPackage.SDM_OPERATIONAL_TGG_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiomImpl <em>mote Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiomImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getmoteAxiom()
	 * @generated
	 */
	int MOTE_AXIOM = 1;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Axioms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__AXIOMS = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__AXIOMS;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__OPERATIONAL_TGG;

	/**
	 * The number of structural features of the '<em>mote Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>mote Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connectionImpl <em>connection2connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connectionImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnection2connection()
	 * @generated
	 */
	int CONNECTION2CONNECTION = 2;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connection2connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connection2connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionrefImpl <em>connectionref2connectionref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionrefImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF = 3;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connectionref2connectionref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2featureImpl <em>feature2feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2featureImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getfeature2feature()
	 * @generated
	 */
	int FEATURE2FEATURE = 4;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>feature2feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>feature2feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponentImpl <em>subcomponent2subcomponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponentImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent()
	 * @generated
	 */
	int SUBCOMPONENT2SUBCOMPONENT = 5;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>subcomponent2subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>subcomponent2subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiom_r0Impl <em>mote Axiom r0</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiom_r0Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getmoteAxiom_r0()
	 * @generated
	 */
	int MOTE_AXIOM_R0 = 6;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_AXIOM__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__RULE_ID = OperationalTGGPackage.OPERATIONAL_AXIOM__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Axiom Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__OPERATIONAL_AXIOM_GROUP = OperationalTGGPackage.OPERATIONAL_AXIOM__OPERATIONAL_AXIOM_GROUP;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>mote Axiom r0</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_AXIOM___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___ADD_ELEMENT__EMAP_ELIST_ELIST = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_FORWARD__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_MAPPING__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_BACKWARD__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___SYNCHRONIZE_FORWARD__ELIST_ELIST_TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___SYNCHRONIZE_BACKWARD__ELIST_ELIST_TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 6;

	/**
	 * The number of operations of the '<em>mote Axiom r0</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionref_r60ffImpl <em>connectionref2connectionref r60ff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionref_r60ffImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF = 7;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref r60ff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connectionref2connectionref r60ff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R60FF_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2feature_r3Impl <em>feature2feature r3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2feature_r3Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getfeature2feature_r3()
	 * @generated
	 */
	int FEATURE2FEATURE_R3 = 8;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>feature2feature r3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>feature2feature r3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponent_r2Impl <em>subcomponent2subcomponent r2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponent_r2Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2 = 9;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>subcomponent2subcomponent r2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>subcomponent2subcomponent r2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connection_r90pImpl <em>connection2connection r90p</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connection_r90pImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnection2connection_r90p()
	 * @generated
	 */
	int CONNECTION2CONNECTION_R90P = 10;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connection2connection r90p</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connection2connection r90p</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90P_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.copyingOperationalTGG <em>copying Operational TGG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>copying Operational TGG</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.copyingOperationalTGG
	 * @generated
	 */
	EClass getcopyingOperationalTGG();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom <em>mote Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>mote Axiom</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom
	 * @generated
	 */
	EClass getmoteAxiom();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection <em>connection2connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connection2connection</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection
	 * @generated
	 */
	EClass getconnection2connection();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref <em>connectionref2connectionref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref
	 * @generated
	 */
	EClass getconnectionref2connectionref();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature <em>feature2feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>feature2feature</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature
	 * @generated
	 */
	EClass getfeature2feature();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent <em>subcomponent2subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>subcomponent2subcomponent</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent
	 * @generated
	 */
	EClass getsubcomponent2subcomponent();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0 <em>mote Axiom r0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>mote Axiom r0</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0
	 * @generated
	 */
	EClass getmoteAxiom_r0();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getAddElementActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getMoveElementActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getChangeAttributeActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformForwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformMappingActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getTransformBackwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getSynchronizeForwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#getSynchronizeBackwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#addElement(org.eclipse.emf.common.util.EMap, org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#addElement(org.eclipse.emf.common.util.EMap, org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__AddElement__EMap_EList_EList();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformForward__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformMapping(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformMapping(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformMapping__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#transformBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformBackward__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#synchronizeForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#synchronizeForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#synchronizeBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0#synchronizeBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff <em>connectionref2connectionref r60ff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref r60ff</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff
	 * @generated
	 */
	EClass getconnectionref2connectionref_r60ff();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getAddElementActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getMoveElementActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getChangeAttributeActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformForwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformMappingActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getTransformBackwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckForwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckMappingActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getConflictCheckBackwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getSynchronizeForwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getSynchronizeBackwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getRepairForwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#getRepairBackwardActivity()
	 * @see #getconnectionref2connectionref_r60ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r60ff_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r60ff__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3 <em>feature2feature r3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>feature2feature r3</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3
	 * @generated
	 */
	EClass getfeature2feature_r3();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getAddElementActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getMoveElementActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getChangeAttributeActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformMappingActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getTransformBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckMappingActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getConflictCheckBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getSynchronizeForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getSynchronizeBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getRepairForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#getRepairBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getfeature2feature_r3__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2 <em>subcomponent2subcomponent r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>subcomponent2subcomponent r2</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2
	 * @generated
	 */
	EClass getsubcomponent2subcomponent_r2();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getAddElementActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getMoveElementActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getChangeAttributeActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformMappingActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getTransformBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckMappingActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getConflictCheckBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getSynchronizeForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getSynchronizeBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getRepairForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#getRepairBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p <em>connection2connection r90p</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connection2connection r90p</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p
	 * @generated
	 */
	EClass getconnection2connection_r90p();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getAddElementActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getMoveElementActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getChangeAttributeActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformForwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformMappingActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getTransformBackwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckForwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckMappingActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getConflictCheckBackwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getSynchronizeForwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getSynchronizeBackwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getRepairForwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#getRepairBackwardActivity()
	 * @see #getconnection2connection_r90p()
	 * @generated
	 */
	EReference getconnection2connection_r90p_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90p__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GeneratedFactory getGeneratedFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.copyingOperationalTGGImpl <em>copying Operational TGG</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.copyingOperationalTGGImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getcopyingOperationalTGG()
		 * @generated
		 */
		EClass COPYING_OPERATIONAL_TGG = eINSTANCE.getcopyingOperationalTGG();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiomImpl <em>mote Axiom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiomImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getmoteAxiom()
		 * @generated
		 */
		EClass MOTE_AXIOM = eINSTANCE.getmoteAxiom();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connectionImpl <em>connection2connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connectionImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnection2connection()
		 * @generated
		 */
		EClass CONNECTION2CONNECTION = eINSTANCE.getconnection2connection();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionrefImpl <em>connectionref2connectionref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionrefImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref()
		 * @generated
		 */
		EClass CONNECTIONREF2CONNECTIONREF = eINSTANCE.getconnectionref2connectionref();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2featureImpl <em>feature2feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2featureImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getfeature2feature()
		 * @generated
		 */
		EClass FEATURE2FEATURE = eINSTANCE.getfeature2feature();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponentImpl <em>subcomponent2subcomponent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponentImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent()
		 * @generated
		 */
		EClass SUBCOMPONENT2SUBCOMPONENT = eINSTANCE.getsubcomponent2subcomponent();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiom_r0Impl <em>mote Axiom r0</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.moteAxiom_r0Impl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getmoteAxiom_r0()
		 * @generated
		 */
		EClass MOTE_AXIOM_R0 = eINSTANCE.getmoteAxiom_r0();

		/**
		 * The meta object literal for the '<em><b>Add Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__ADD_ELEMENT_ACTIVITY = eINSTANCE.getmoteAxiom_r0_AddElementActivity();

		/**
		 * The meta object literal for the '<em><b>Move Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__MOVE_ELEMENT_ACTIVITY = eINSTANCE.getmoteAxiom_r0_MoveElementActivity();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__CHANGE_ATTRIBUTE_ACTIVITY = eINSTANCE.getmoteAxiom_r0_ChangeAttributeActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__TRANSFORM_FORWARD_ACTIVITY = eINSTANCE.getmoteAxiom_r0_TransformForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__TRANSFORM_MAPPING_ACTIVITY = eINSTANCE.getmoteAxiom_r0_TransformMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__TRANSFORM_BACKWARD_ACTIVITY = eINSTANCE.getmoteAxiom_r0_TransformBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__SYNCHRONIZE_FORWARD_ACTIVITY = eINSTANCE.getmoteAxiom_r0_SynchronizeForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTE_AXIOM_R0__SYNCHRONIZE_BACKWARD_ACTIVITY = eINSTANCE
				.getmoteAxiom_r0_SynchronizeBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Add Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___ADD_ELEMENT__EMAP_ELIST_ELIST = eINSTANCE
				.getmoteAxiom_r0__AddElement__EMap_EList_EList();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = eINSTANCE
				.getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap();

		/**
		 * The meta object literal for the '<em><b>Transform Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___TRANSFORM_FORWARD__ELIST_ELIST_BOOLEAN = eINSTANCE
				.getmoteAxiom_r0__TransformForward__EList_EList_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___TRANSFORM_MAPPING__ELIST_ELIST_BOOLEAN = eINSTANCE
				.getmoteAxiom_r0__TransformMapping__EList_EList_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___TRANSFORM_BACKWARD__ELIST_ELIST_BOOLEAN = eINSTANCE
				.getmoteAxiom_r0__TransformBackward__EList_EList_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___SYNCHRONIZE_FORWARD__ELIST_ELIST_TGGNODE_BOOLEAN = eINSTANCE
				.getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOTE_AXIOM_R0___SYNCHRONIZE_BACKWARD__ELIST_ELIST_TGGNODE_BOOLEAN = eINSTANCE
				.getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionref_r60ffImpl <em>connectionref2connectionref r60ff</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connectionref2connectionref_r60ffImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref_r60ff()
		 * @generated
		 */
		EClass CONNECTIONREF2CONNECTIONREF_R60FF = eINSTANCE.getconnectionref2connectionref_r60ff();

		/**
		 * The meta object literal for the '<em><b>Add Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__ADD_ELEMENT_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_AddElementActivity();

		/**
		 * The meta object literal for the '<em><b>Move Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__MOVE_ELEMENT_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_MoveElementActivity();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__CHANGE_ATTRIBUTE_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_ChangeAttributeActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_FORWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_TransformForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_MAPPING_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_TransformMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_BACKWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_TransformBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_FORWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_ConflictCheckForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_MAPPING_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_ConflictCheckMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_BACKWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_ConflictCheckBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_FORWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_SynchronizeForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_BACKWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_SynchronizeBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_FORWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_RepairForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_BACKWARD_ACTIVITY = eINSTANCE
				.getconnectionref2connectionref_r60ff_RepairBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Add Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___ADD_ELEMENT__EMAP = eINSTANCE
				.getconnectionref2connectionref_r60ff__AddElement__EMap();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = eINSTANCE
				.getconnectionref2connectionref_r60ff__ChangeAttributeValues__TGGNode_EMap();

		/**
		 * The meta object literal for the '<em><b>Move Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = eINSTANCE
				.getconnectionref2connectionref_r60ff__MoveElement__TGGNode_TGGNode_TGGNode();

		/**
		 * The meta object literal for the '<em><b>Transform Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__TransformForward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__TransformMapping__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__TransformBackward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_FORWARD__TGGNODE = eINSTANCE
				.getconnectionref2connectionref_r60ff__ConflictCheckForward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_MAPPING__TGGNODE = eINSTANCE
				.getconnectionref2connectionref_r60ff__ConflictCheckMapping__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_BACKWARD__TGGNODE = eINSTANCE
				.getconnectionref2connectionref_r60ff__ConflictCheckBackward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__SynchronizeForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__SynchronizeBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__RepairForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnectionref2connectionref_r60ff__RepairBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2feature_r3Impl <em>feature2feature r3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.feature2feature_r3Impl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getfeature2feature_r3()
		 * @generated
		 */
		EClass FEATURE2FEATURE_R3 = eINSTANCE.getfeature2feature_r3();

		/**
		 * The meta object literal for the '<em><b>Add Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__ADD_ELEMENT_ACTIVITY = eINSTANCE.getfeature2feature_r3_AddElementActivity();

		/**
		 * The meta object literal for the '<em><b>Move Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__MOVE_ELEMENT_ACTIVITY = eINSTANCE.getfeature2feature_r3_MoveElementActivity();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__CHANGE_ATTRIBUTE_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_ChangeAttributeActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__TRANSFORM_FORWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_TransformForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__TRANSFORM_MAPPING_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_TransformMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__TRANSFORM_BACKWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_TransformBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__CONFLICT_CHECK_FORWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_ConflictCheckForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__CONFLICT_CHECK_MAPPING_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_ConflictCheckMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__CONFLICT_CHECK_BACKWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_ConflictCheckBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__SYNCHRONIZE_FORWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_SynchronizeForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__SYNCHRONIZE_BACKWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_SynchronizeBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__REPAIR_FORWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_RepairForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2FEATURE_R3__REPAIR_BACKWARD_ACTIVITY = eINSTANCE
				.getfeature2feature_r3_RepairBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Add Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___ADD_ELEMENT__EMAP = eINSTANCE.getfeature2feature_r3__AddElement__EMap();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = eINSTANCE
				.getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap();

		/**
		 * The meta object literal for the '<em><b>Move Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = eINSTANCE
				.getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode();

		/**
		 * The meta object literal for the '<em><b>Transform Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___CONFLICT_CHECK_FORWARD__TGGNODE = eINSTANCE
				.getfeature2feature_r3__ConflictCheckForward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___CONFLICT_CHECK_MAPPING__TGGNODE = eINSTANCE
				.getfeature2feature_r3__ConflictCheckMapping__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___CONFLICT_CHECK_BACKWARD__TGGNODE = eINSTANCE
				.getfeature2feature_r3__ConflictCheckBackward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__SynchronizeForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___REPAIR_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__RepairForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FEATURE2FEATURE_R3___REPAIR_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getfeature2feature_r3__RepairBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponent_r2Impl <em>subcomponent2subcomponent r2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.subcomponent2subcomponent_r2Impl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent_r2()
		 * @generated
		 */
		EClass SUBCOMPONENT2SUBCOMPONENT_R2 = eINSTANCE.getsubcomponent2subcomponent_r2();

		/**
		 * The meta object literal for the '<em><b>Add Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__ADD_ELEMENT_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_AddElementActivity();

		/**
		 * The meta object literal for the '<em><b>Move Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__MOVE_ELEMENT_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_MoveElementActivity();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__CHANGE_ATTRIBUTE_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_ChangeAttributeActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_FORWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_TransformForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_MAPPING_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_TransformMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_BACKWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_TransformBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_FORWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_MAPPING_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_BACKWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_FORWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_SynchronizeForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_BACKWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_FORWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_RepairForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_BACKWARD_ACTIVITY = eINSTANCE
				.getsubcomponent2subcomponent_r2_RepairBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Add Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___ADD_ELEMENT__EMAP = eINSTANCE
				.getsubcomponent2subcomponent_r2__AddElement__EMap();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = eINSTANCE
				.getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap();

		/**
		 * The meta object literal for the '<em><b>Move Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = eINSTANCE
				.getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode();

		/**
		 * The meta object literal for the '<em><b>Transform Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_FORWARD__TGGNODE = eINSTANCE
				.getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_MAPPING__TGGNODE = eINSTANCE
				.getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_BACKWARD__TGGNODE = eINSTANCE
				.getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connection_r90pImpl <em>connection2connection r90p</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.connection2connection_r90pImpl
		 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl#getconnection2connection_r90p()
		 * @generated
		 */
		EClass CONNECTION2CONNECTION_R90P = eINSTANCE.getconnection2connection_r90p();

		/**
		 * The meta object literal for the '<em><b>Add Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__ADD_ELEMENT_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_AddElementActivity();

		/**
		 * The meta object literal for the '<em><b>Move Element Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__MOVE_ELEMENT_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_MoveElementActivity();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__CHANGE_ATTRIBUTE_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_ChangeAttributeActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__TRANSFORM_FORWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_TransformForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__TRANSFORM_MAPPING_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_TransformMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Transform Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__TRANSFORM_BACKWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_TransformBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_FORWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_ConflictCheckForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_MAPPING_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_ConflictCheckMappingActivity();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_BACKWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_ConflictCheckBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__SYNCHRONIZE_FORWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_SynchronizeForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__SYNCHRONIZE_BACKWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_SynchronizeBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Forward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__REPAIR_FORWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_RepairForwardActivity();

		/**
		 * The meta object literal for the '<em><b>Repair Backward Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION2CONNECTION_R90P__REPAIR_BACKWARD_ACTIVITY = eINSTANCE
				.getconnection2connection_r90p_RepairBackwardActivity();

		/**
		 * The meta object literal for the '<em><b>Add Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___ADD_ELEMENT__EMAP = eINSTANCE
				.getconnection2connection_r90p__AddElement__EMap();

		/**
		 * The meta object literal for the '<em><b>Change Attribute Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = eINSTANCE
				.getconnection2connection_r90p__ChangeAttributeValues__TGGNode_EMap();

		/**
		 * The meta object literal for the '<em><b>Move Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = eINSTANCE
				.getconnection2connection_r90p__MoveElement__TGGNode_TGGNode_TGGNode();

		/**
		 * The meta object literal for the '<em><b>Transform Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__TransformForward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__TransformMapping__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Transform Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__TransformBackward__TGGNode_boolean_boolean();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_FORWARD__TGGNODE = eINSTANCE
				.getconnection2connection_r90p__ConflictCheckForward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Mapping</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_MAPPING__TGGNODE = eINSTANCE
				.getconnection2connection_r90p__ConflictCheckMapping__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Conflict Check Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_BACKWARD__TGGNODE = eINSTANCE
				.getconnection2connection_r90p__ConflictCheckBackward__TGGNode();

		/**
		 * The meta object literal for the '<em><b>Synchronize Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__SynchronizeForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Synchronize Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__SynchronizeBackward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___REPAIR_FORWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__RepairForward__TGGNode_boolean();

		/**
		 * The meta object literal for the '<em><b>Repair Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONNECTION2CONNECTION_R90P___REPAIR_BACKWARD__TGGNODE_BOOLEAN = eINSTANCE
				.getconnection2connection_r90p__RepairBackward__TGGNode_boolean();

	}

} //GeneratedPackage
