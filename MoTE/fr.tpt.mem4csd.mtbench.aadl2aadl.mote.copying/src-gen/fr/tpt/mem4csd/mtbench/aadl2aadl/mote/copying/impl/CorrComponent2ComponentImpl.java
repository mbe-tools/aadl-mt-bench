/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrComponent2Component;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Component2 Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrComponent2ComponentImpl extends CorrFeature2FeatureImpl implements CorrComponent2Component {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrComponent2ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.CORR_COMPONENT2_COMPONENT;
	}

} //CorrComponent2ComponentImpl
