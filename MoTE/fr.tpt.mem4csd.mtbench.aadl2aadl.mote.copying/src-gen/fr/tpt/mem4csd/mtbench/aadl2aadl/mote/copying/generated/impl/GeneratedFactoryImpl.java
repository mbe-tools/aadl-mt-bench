/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GeneratedFactoryImpl extends EFactoryImpl implements GeneratedFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GeneratedFactory init() {
		try {
			GeneratedFactory theGeneratedFactory = (GeneratedFactory) EPackage.Registry.INSTANCE
					.getEFactory(GeneratedPackage.eNS_URI);
			if (theGeneratedFactory != null) {
				return theGeneratedFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GeneratedFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case GeneratedPackage.COPYING_OPERATIONAL_TGG:
			return createcopyingOperationalTGG();
		case GeneratedPackage.MOTE_AXIOM:
			return createmoteAxiom();
		case GeneratedPackage.CONNECTION2CONNECTION:
			return createconnection2connection();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF:
			return createconnectionref2connectionref();
		case GeneratedPackage.FEATURE2FEATURE:
			return createfeature2feature();
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT:
			return createsubcomponent2subcomponent();
		case GeneratedPackage.MOTE_AXIOM_R0:
			return createmoteAxiom_r0();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF_R60FF:
			return createconnectionref2connectionref_r60ff();
		case GeneratedPackage.FEATURE2FEATURE_R3:
			return createfeature2feature_r3();
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT_R2:
			return createsubcomponent2subcomponent_r2();
		case GeneratedPackage.CONNECTION2CONNECTION_R90P:
			return createconnection2connection_r90p();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public copyingOperationalTGG createcopyingOperationalTGG() {
		copyingOperationalTGGImpl copyingOperationalTGG = new copyingOperationalTGGImpl();
		return copyingOperationalTGG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public moteAxiom createmoteAxiom() {
		moteAxiomImpl moteAxiom = new moteAxiomImpl();
		return moteAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connection2connection createconnection2connection() {
		connection2connectionImpl connection2connection = new connection2connectionImpl();
		return connection2connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref createconnectionref2connectionref() {
		connectionref2connectionrefImpl connectionref2connectionref = new connectionref2connectionrefImpl();
		return connectionref2connectionref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public feature2feature createfeature2feature() {
		feature2featureImpl feature2feature = new feature2featureImpl();
		return feature2feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public subcomponent2subcomponent createsubcomponent2subcomponent() {
		subcomponent2subcomponentImpl subcomponent2subcomponent = new subcomponent2subcomponentImpl();
		return subcomponent2subcomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public moteAxiom_r0 createmoteAxiom_r0() {
		moteAxiom_r0Impl moteAxiom_r0 = new moteAxiom_r0Impl();
		return moteAxiom_r0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref_r60ff createconnectionref2connectionref_r60ff() {
		connectionref2connectionref_r60ffImpl connectionref2connectionref_r60ff = new connectionref2connectionref_r60ffImpl();
		return connectionref2connectionref_r60ff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public feature2feature_r3 createfeature2feature_r3() {
		feature2feature_r3Impl feature2feature_r3 = new feature2feature_r3Impl();
		return feature2feature_r3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public subcomponent2subcomponent_r2 createsubcomponent2subcomponent_r2() {
		subcomponent2subcomponent_r2Impl subcomponent2subcomponent_r2 = new subcomponent2subcomponent_r2Impl();
		return subcomponent2subcomponent_r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connection2connection_r90p createconnection2connection_r90p() {
		connection2connection_r90pImpl connection2connection_r90p = new connection2connection_r90pImpl();
		return connection2connection_r90p;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedPackage getGeneratedPackage() {
		return (GeneratedPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GeneratedPackage getPackage() {
		return GeneratedPackage.eINSTANCE;
	}

} //GeneratedFactoryImpl
