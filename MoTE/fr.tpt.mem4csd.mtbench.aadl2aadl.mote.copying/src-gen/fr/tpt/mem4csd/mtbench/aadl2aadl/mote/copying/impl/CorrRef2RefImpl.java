/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import de.mdelab.mltgg.mote2.impl.TGGNodeImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrRef2Ref;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Ref2 Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrRef2RefImpl extends TGGNodeImpl implements CorrRef2Ref {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrRef2RefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.CORR_REF2_REF;
	}

} //CorrRef2RefImpl
