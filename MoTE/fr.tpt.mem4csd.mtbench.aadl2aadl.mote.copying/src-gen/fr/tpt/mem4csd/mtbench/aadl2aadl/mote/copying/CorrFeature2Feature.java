/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying;

import de.mdelab.mltgg.mote2.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Feature2 Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage#getCorrFeature2Feature()
 * @model
 * @generated
 */
public interface CorrFeature2Feature extends TGGNode {
} // CorrFeature2Feature
