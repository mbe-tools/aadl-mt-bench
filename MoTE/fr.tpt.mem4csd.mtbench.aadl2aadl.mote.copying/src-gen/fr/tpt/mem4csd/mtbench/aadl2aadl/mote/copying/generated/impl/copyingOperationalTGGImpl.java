/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl;

import de.mdelab.mltgg.mote2.sdm.impl.SdmOperationalTGGImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.copyingOperationalTGG;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>copying Operational TGG</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class copyingOperationalTGGImpl extends SdmOperationalTGGImpl implements copyingOperationalTGG {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected copyingOperationalTGGImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratedPackage.Literals.COPYING_OPERATIONAL_TGG;
	}

} //copyingOperationalTGGImpl
