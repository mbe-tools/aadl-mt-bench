/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import de.mdelab.mltgg.mote2.impl.TGGNodeImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrFeature2Feature;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Feature2 Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrFeature2FeatureImpl extends TGGNodeImpl implements CorrFeature2Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrFeature2FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.CORR_FEATURE2_FEATURE;
	}

} //CorrFeature2FeatureImpl
