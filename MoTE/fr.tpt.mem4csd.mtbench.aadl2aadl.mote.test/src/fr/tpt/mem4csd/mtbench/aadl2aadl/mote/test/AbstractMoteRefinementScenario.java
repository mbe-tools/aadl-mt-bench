package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import de.mdelab.mltgg.mote2.sdm.MoTE2Sdm;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage;

public abstract class AbstractMoteRefinementScenario extends AbstractMoteScenario {

	protected AbstractMoteRefinementScenario() {
		super();
	}

	@Override
	protected void setup() {
		super.setup();
		
		Aadl2aadlPackage.eINSTANCE.eClass();
	}

	@Override
	protected MoTE2Sdm createEngine() {
		return super.createEngine( "../fr.tpt.mem4csd.mtbench.aadl2aadl.mote.refinement/model-gen/config" );
	}
}