package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementBatch extends AbstractMoteRefinementScenario {

	public RefinementBatch() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.BATCH;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_BATCH_SUFFIX;
	}


}
