package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingBatch extends AbstractMoteCopyingScenario {

	public CopyingBatch() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.BATCH;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_BATCH_SUFFIX;
	}



}
