package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingAddition extends AbstractMoteCopyingScenario {

	public CopyingAddition() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_ADD_SUFFIX;
	}

}
