/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Conn2 Conn</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage#getCorrConn2Conn()
 * @model
 * @generated
 */
public interface CorrConn2Conn extends CorrFeature2Feature {
} // CorrConn2Conn
