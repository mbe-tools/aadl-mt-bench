/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.util;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiom;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiomGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMapping;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMappingGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRule;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGG;

import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage
 * @generated
 */
public class GeneratedSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GeneratedPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedSwitch() {
		if (modelPackage == null) {
			modelPackage = GeneratedPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case GeneratedPackage.AADL2AADL_OPERATIONAL_TGG: {
			aadl2aadlOperationalTGG aadl2aadlOperationalTGG = (aadl2aadlOperationalTGG) theEObject;
			T result = caseaadl2aadlOperationalTGG(aadl2aadlOperationalTGG);
			if (result == null)
				result = caseSdmOperationalTGG(aadl2aadlOperationalTGG);
			if (result == null)
				result = caseOperationalTGG(aadl2aadlOperationalTGG);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.MOTE_AXIOM: {
			moteAxiom moteAxiom = (moteAxiom) theEObject;
			T result = casemoteAxiom(moteAxiom);
			if (result == null)
				result = caseOperationalAxiomGroup(moteAxiom);
			if (result == null)
				result = caseOperationalMappingGroup(moteAxiom);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE10: {
			connectionref2connectionref4device10 connectionref2connectionref4device10 = (connectionref2connectionref4device10) theEObject;
			T result = caseconnectionref2connectionref4device10(connectionref2connectionref4device10);
			if (result == null)
				result = caseOperationalRuleGroup(connectionref2connectionref4device10);
			if (result == null)
				result = caseOperationalMappingGroup(connectionref2connectionref4device10);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION: {
			portconnection2portconnection4deviceDestination portconnection2portconnection4deviceDestination = (portconnection2portconnection4deviceDestination) theEObject;
			T result = caseportconnection2portconnection4deviceDestination(
					portconnection2portconnection4deviceDestination);
			if (result == null)
				result = caseOperationalRuleGroup(portconnection2portconnection4deviceDestination);
			if (result == null)
				result = caseOperationalMappingGroup(portconnection2portconnection4deviceDestination);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE01: {
			connectionref2connectionref4device01 connectionref2connectionref4device01 = (connectionref2connectionref4device01) theEObject;
			T result = caseconnectionref2connectionref4device01(connectionref2connectionref4device01);
			if (result == null)
				result = caseOperationalRuleGroup(connectionref2connectionref4device01);
			if (result == null)
				result = caseOperationalMappingGroup(connectionref2connectionref4device01);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.FEATURE2FEATURE: {
			feature2feature feature2feature = (feature2feature) theEObject;
			T result = casefeature2feature(feature2feature);
			if (result == null)
				result = caseOperationalRuleGroup(feature2feature);
			if (result == null)
				result = caseOperationalMappingGroup(feature2feature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS01: {
			PortConnection2DataAccess01 portConnection2DataAccess01 = (PortConnection2DataAccess01) theEObject;
			T result = casePortConnection2DataAccess01(portConnection2DataAccess01);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccess01);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccess01);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF: {
			connectionref2connectionref connectionref2connectionref = (connectionref2connectionref) theEObject;
			T result = caseconnectionref2connectionref(connectionref2connectionref);
			if (result == null)
				result = caseOperationalRuleGroup(connectionref2connectionref);
			if (result == null)
				result = caseOperationalMappingGroup(connectionref2connectionref);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS01: {
			PortConnection2DataAccessSys01 portConnection2DataAccessSys01 = (PortConnection2DataAccessSys01) theEObject;
			T result = casePortConnection2DataAccessSys01(portConnection2DataAccessSys01);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccessSys01);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccessSys01);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE11: {
			connectionref2connectionref4device11 connectionref2connectionref4device11 = (connectionref2connectionref4device11) theEObject;
			T result = caseconnectionref2connectionref4device11(connectionref2connectionref4device11);
			if (result == null)
				result = caseOperationalRuleGroup(connectionref2connectionref4device11);
			if (result == null)
				result = caseOperationalMappingGroup(connectionref2connectionref4device11);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS00: {
			PortConnection2DataAccessSys00 portConnection2DataAccessSys00 = (PortConnection2DataAccessSys00) theEObject;
			T result = casePortConnection2DataAccessSys00(portConnection2DataAccessSys00);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccessSys00);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccessSys00);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS11: {
			PortConnection2DataAccess11 portConnection2DataAccess11 = (PortConnection2DataAccess11) theEObject;
			T result = casePortConnection2DataAccess11(portConnection2DataAccess11);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccess11);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccess11);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS10: {
			PortConnection2DataAccess10 portConnection2DataAccess10 = (PortConnection2DataAccess10) theEObject;
			T result = casePortConnection2DataAccess10(portConnection2DataAccess10);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccess10);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccess10);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE: {
			portconnection2portconnection4deviceSource portconnection2portconnection4deviceSource = (portconnection2portconnection4deviceSource) theEObject;
			T result = caseportconnection2portconnection4deviceSource(portconnection2portconnection4deviceSource);
			if (result == null)
				result = caseOperationalRuleGroup(portconnection2portconnection4deviceSource);
			if (result == null)
				result = caseOperationalMappingGroup(portconnection2portconnection4deviceSource);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTION2CONNECTION: {
			connection2connection connection2connection = (connection2connection) theEObject;
			T result = caseconnection2connection(connection2connection);
			if (result == null)
				result = caseOperationalRuleGroup(connection2connection);
			if (result == null)
				result = caseOperationalMappingGroup(connection2connection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS11: {
			PortConnection2DataAccessSys11 portConnection2DataAccessSys11 = (PortConnection2DataAccessSys11) theEObject;
			T result = casePortConnection2DataAccessSys11(portConnection2DataAccessSys11);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccessSys11);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccessSys11);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS00: {
			PortConnection2DataAccess00 portConnection2DataAccess00 = (PortConnection2DataAccess00) theEObject;
			T result = casePortConnection2DataAccess00(portConnection2DataAccess00);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccess00);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccess00);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT: {
			subcomponent2subcomponent subcomponent2subcomponent = (subcomponent2subcomponent) theEObject;
			T result = casesubcomponent2subcomponent(subcomponent2subcomponent);
			if (result == null)
				result = caseOperationalRuleGroup(subcomponent2subcomponent);
			if (result == null)
				result = caseOperationalMappingGroup(subcomponent2subcomponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS10: {
			PortConnection2DataAccessSys10 portConnection2DataAccessSys10 = (PortConnection2DataAccessSys10) theEObject;
			T result = casePortConnection2DataAccessSys10(portConnection2DataAccessSys10);
			if (result == null)
				result = caseOperationalRuleGroup(portConnection2DataAccessSys10);
			if (result == null)
				result = caseOperationalMappingGroup(portConnection2DataAccessSys10);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.MOTE_AXIOM_R0: {
			moteAxiom_r0 moteAxiom_r0 = (moteAxiom_r0) theEObject;
			T result = casemoteAxiom_r0(moteAxiom_r0);
			if (result == null)
				result = caseOperationalAxiom(moteAxiom_r0);
			if (result == null)
				result = caseOperationalMapping(moteAxiom_r0);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS01_R401: {
			PortConnection2DataAccess01_r401 portConnection2DataAccess01_r401 = (PortConnection2DataAccess01_r401) theEObject;
			T result = casePortConnection2DataAccess01_r401(portConnection2DataAccess01_r401);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccess01_r401);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccess01_r401);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTION2CONNECTION_R90: {
			connection2connection_r90 connection2connection_r90 = (connection2connection_r90) theEObject;
			T result = caseconnection2connection_r90(connection2connection_r90);
			if (result == null)
				result = caseOperationalRule(connection2connection_r90);
			if (result == null)
				result = caseOperationalMapping(connection2connection_r90);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD: {
			portconnection2portconnection4deviceDestination_r90DD portconnection2portconnection4deviceDestination_r90DD = (portconnection2portconnection4deviceDestination_r90DD) theEObject;
			T result = caseportconnection2portconnection4deviceDestination_r90DD(
					portconnection2portconnection4deviceDestination_r90DD);
			if (result == null)
				result = caseOperationalRule(portconnection2portconnection4deviceDestination_r90DD);
			if (result == null)
				result = caseOperationalMapping(portconnection2portconnection4deviceDestination_r90DD);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS11_R411: {
			PortConnection2DataAccess11_r411 portConnection2DataAccess11_r411 = (PortConnection2DataAccess11_r411) theEObject;
			T result = casePortConnection2DataAccess11_r411(portConnection2DataAccess11_r411);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccess11_r411);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccess11_r411);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS10_R810: {
			PortConnection2DataAccessSys10_r810 portConnection2DataAccessSys10_r810 = (PortConnection2DataAccessSys10_r810) theEObject;
			T result = casePortConnection2DataAccessSys10_r810(portConnection2DataAccessSys10_r810);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccessSys10_r810);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccessSys10_r810);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS01_R801: {
			PortConnection2DataAccessSys01_r801 portConnection2DataAccessSys01_r801 = (PortConnection2DataAccessSys01_r801) theEObject;
			T result = casePortConnection2DataAccessSys01_r801(portConnection2DataAccessSys01_r801);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccessSys01_r801);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccessSys01_r801);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10: {
			connectionref2connectionref4device10_r61ffd10 connectionref2connectionref4device10_r61ffd10 = (connectionref2connectionref4device10_r61ffd10) theEObject;
			T result = caseconnectionref2connectionref4device10_r61ffd10(connectionref2connectionref4device10_r61ffd10);
			if (result == null)
				result = caseOperationalRule(connectionref2connectionref4device10_r61ffd10);
			if (result == null)
				result = caseOperationalMapping(connectionref2connectionref4device10_r61ffd10);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT_R2: {
			subcomponent2subcomponent_r2 subcomponent2subcomponent_r2 = (subcomponent2subcomponent_r2) theEObject;
			T result = casesubcomponent2subcomponent_r2(subcomponent2subcomponent_r2);
			if (result == null)
				result = caseOperationalRule(subcomponent2subcomponent_r2);
			if (result == null)
				result = caseOperationalMapping(subcomponent2subcomponent_r2);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS00_R400: {
			PortConnection2DataAccess00_r400 portConnection2DataAccess00_r400 = (PortConnection2DataAccess00_r400) theEObject;
			T result = casePortConnection2DataAccess00_r400(portConnection2DataAccess00_r400);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccess00_r400);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccess00_r400);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS00_R800: {
			PortConnection2DataAccessSys00_r800 portConnection2DataAccessSys00_r800 = (PortConnection2DataAccessSys00_r800) theEObject;
			T result = casePortConnection2DataAccessSys00_r800(portConnection2DataAccessSys00_r800);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccessSys00_r800);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccessSys00_r800);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF_R61FF: {
			connectionref2connectionref_r61ff connectionref2connectionref_r61ff = (connectionref2connectionref_r61ff) theEObject;
			T result = caseconnectionref2connectionref_r61ff(connectionref2connectionref_r61ff);
			if (result == null)
				result = caseOperationalRule(connectionref2connectionref_r61ff);
			if (result == null)
				result = caseOperationalMapping(connectionref2connectionref_r61ff);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11: {
			connectionref2connectionref4device11_r61ffd11 connectionref2connectionref4device11_r61ffd11 = (connectionref2connectionref4device11_r61ffd11) theEObject;
			T result = caseconnectionref2connectionref4device11_r61ffd11(connectionref2connectionref4device11_r61ffd11);
			if (result == null)
				result = caseOperationalRule(connectionref2connectionref4device11_r61ffd11);
			if (result == null)
				result = caseOperationalMapping(connectionref2connectionref4device11_r61ffd11);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS10_R410: {
			PortConnection2DataAccess10_r410 portConnection2DataAccess10_r410 = (PortConnection2DataAccess10_r410) theEObject;
			T result = casePortConnection2DataAccess10_r410(portConnection2DataAccess10_r410);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccess10_r410);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccess10_r410);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.FEATURE2FEATURE_R3: {
			feature2feature_r3 feature2feature_r3 = (feature2feature_r3) theEObject;
			T result = casefeature2feature_r3(feature2feature_r3);
			if (result == null)
				result = caseOperationalRule(feature2feature_r3);
			if (result == null)
				result = caseOperationalMapping(feature2feature_r3);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS11_R811: {
			PortConnection2DataAccessSys11_r811 portConnection2DataAccessSys11_r811 = (PortConnection2DataAccessSys11_r811) theEObject;
			T result = casePortConnection2DataAccessSys11_r811(portConnection2DataAccessSys11_r811);
			if (result == null)
				result = caseOperationalRule(portConnection2DataAccessSys11_r811);
			if (result == null)
				result = caseOperationalMapping(portConnection2DataAccessSys11_r811);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01: {
			connectionref2connectionref4device01_r61ffd01 connectionref2connectionref4device01_r61ffd01 = (connectionref2connectionref4device01_r61ffd01) theEObject;
			T result = caseconnectionref2connectionref4device01_r61ffd01(connectionref2connectionref4device01_r61ffd01);
			if (result == null)
				result = caseOperationalRule(connectionref2connectionref4device01_r61ffd01);
			if (result == null)
				result = caseOperationalMapping(connectionref2connectionref4device01_r61ffd01);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS: {
			portconnection2portconnection4deviceSource_r90DS portconnection2portconnection4deviceSource_r90DS = (portconnection2portconnection4deviceSource_r90DS) theEObject;
			T result = caseportconnection2portconnection4deviceSource_r90DS(
					portconnection2portconnection4deviceSource_r90DS);
			if (result == null)
				result = caseOperationalRule(portconnection2portconnection4deviceSource_r90DS);
			if (result == null)
				result = caseOperationalMapping(portconnection2portconnection4deviceSource_r90DS);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>aadl2aadl Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>aadl2aadl Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseaadl2aadlOperationalTGG(aadl2aadlOperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>mote Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>mote Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casemoteAxiom(moteAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device10(connectionref2connectionref4device10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Destination</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Destination</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseportconnection2portconnection4deviceDestination(
			portconnection2portconnection4deviceDestination object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device01</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device01</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device01(connectionref2connectionref4device01 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>feature2feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>feature2feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefeature2feature(feature2feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access01</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access01</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess01(PortConnection2DataAccess01 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref(connectionref2connectionref object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys01</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys01</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys01(PortConnection2DataAccessSys01 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device11(connectionref2connectionref4device11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys00</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys00</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys00(PortConnection2DataAccessSys00 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess11(PortConnection2DataAccess11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess10(PortConnection2DataAccess10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseportconnection2portconnection4deviceSource(portconnection2portconnection4deviceSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connection2connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connection2connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnection2connection(connection2connection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys11(PortConnection2DataAccessSys11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access00</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access00</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess00(PortConnection2DataAccess00 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>subcomponent2subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>subcomponent2subcomponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesubcomponent2subcomponent(subcomponent2subcomponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys10(PortConnection2DataAccessSys10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>mote Axiom r0</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>mote Axiom r0</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casemoteAxiom_r0(moteAxiom_r0 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access01 r401</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access01 r401</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess01_r401(PortConnection2DataAccess01_r401 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connection2connection r90</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connection2connection r90</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnection2connection_r90(connection2connection_r90 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Destination r90 DD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Destination r90 DD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseportconnection2portconnection4deviceDestination_r90DD(
			portconnection2portconnection4deviceDestination_r90DD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access11 r411</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access11 r411</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess11_r411(PortConnection2DataAccess11_r411 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys10 r810</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys10 r810</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys10_r810(PortConnection2DataAccessSys10_r810 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys01 r801</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys01 r801</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys01_r801(PortConnection2DataAccessSys01_r801 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device10 r61ffd10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device10 r61ffd10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device10_r61ffd10(connectionref2connectionref4device10_r61ffd10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>subcomponent2subcomponent r2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>subcomponent2subcomponent r2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesubcomponent2subcomponent_r2(subcomponent2subcomponent_r2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access00 r400</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access00 r400</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess00_r400(PortConnection2DataAccess00_r400 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys00 r800</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys00 r800</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys00_r800(PortConnection2DataAccessSys00_r800 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref r61ff</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref r61ff</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref_r61ff(connectionref2connectionref_r61ff object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device11 r61ffd11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device11 r61ffd11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device11_r61ffd11(connectionref2connectionref4device11_r61ffd11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access10 r410</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access10 r410</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccess10_r410(PortConnection2DataAccess10_r410 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>feature2feature r3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>feature2feature r3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefeature2feature_r3(feature2feature_r3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys11 r811</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection2 Data Access Sys11 r811</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnection2DataAccessSys11_r811(PortConnection2DataAccessSys11_r811 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref4device01 r61ffd01</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref4device01 r61ffd01</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref4device01_r61ffd01(connectionref2connectionref4device01_r61ffd01 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Source r90 DS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>portconnection2portconnection4device Source r90 DS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseportconnection2portconnection4deviceSource_r90DS(
			portconnection2portconnection4deviceSource_r90DS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalTGG(OperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSdmOperationalTGG(SdmOperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Mapping Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Mapping Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalMappingGroup(OperationalMappingGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Axiom Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Axiom Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalAxiomGroup(OperationalAxiomGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Rule Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Rule Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalRuleGroup(OperationalRuleGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalMapping(OperationalMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalAxiom(OperationalAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalRule(OperationalRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //GeneratedSwitch
