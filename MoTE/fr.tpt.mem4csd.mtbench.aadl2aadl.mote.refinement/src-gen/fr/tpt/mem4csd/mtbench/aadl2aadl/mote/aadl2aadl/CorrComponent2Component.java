/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Component2 Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage#getCorrComponent2Component()
 * @model
 * @generated
 */
public interface CorrComponent2Component extends CorrFeature2Feature {
} // CorrComponent2Component
