/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage
 * @generated
 */
public interface GeneratedFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneratedFactory eINSTANCE = fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>aadl2aadl Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>aadl2aadl Operational TGG</em>'.
	 * @generated
	 */
	aadl2aadlOperationalTGG createaadl2aadlOperationalTGG();

	/**
	 * Returns a new object of class '<em>mote Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>mote Axiom</em>'.
	 * @generated
	 */
	moteAxiom createmoteAxiom();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device10</em>'.
	 * @generated
	 */
	connectionref2connectionref4device10 createconnectionref2connectionref4device10();

	/**
	 * Returns a new object of class '<em>portconnection2portconnection4device Destination</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>portconnection2portconnection4device Destination</em>'.
	 * @generated
	 */
	portconnection2portconnection4deviceDestination createportconnection2portconnection4deviceDestination();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device01</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device01</em>'.
	 * @generated
	 */
	connectionref2connectionref4device01 createconnectionref2connectionref4device01();

	/**
	 * Returns a new object of class '<em>feature2feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>feature2feature</em>'.
	 * @generated
	 */
	feature2feature createfeature2feature();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access01</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access01</em>'.
	 * @generated
	 */
	PortConnection2DataAccess01 createPortConnection2DataAccess01();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref</em>'.
	 * @generated
	 */
	connectionref2connectionref createconnectionref2connectionref();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys01</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys01</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys01 createPortConnection2DataAccessSys01();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device11</em>'.
	 * @generated
	 */
	connectionref2connectionref4device11 createconnectionref2connectionref4device11();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys00</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys00</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys00 createPortConnection2DataAccessSys00();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access11</em>'.
	 * @generated
	 */
	PortConnection2DataAccess11 createPortConnection2DataAccess11();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access10</em>'.
	 * @generated
	 */
	PortConnection2DataAccess10 createPortConnection2DataAccess10();

	/**
	 * Returns a new object of class '<em>portconnection2portconnection4device Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>portconnection2portconnection4device Source</em>'.
	 * @generated
	 */
	portconnection2portconnection4deviceSource createportconnection2portconnection4deviceSource();

	/**
	 * Returns a new object of class '<em>connection2connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connection2connection</em>'.
	 * @generated
	 */
	connection2connection createconnection2connection();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys11</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys11 createPortConnection2DataAccessSys11();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access00</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access00</em>'.
	 * @generated
	 */
	PortConnection2DataAccess00 createPortConnection2DataAccess00();

	/**
	 * Returns a new object of class '<em>subcomponent2subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>subcomponent2subcomponent</em>'.
	 * @generated
	 */
	subcomponent2subcomponent createsubcomponent2subcomponent();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys10</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys10 createPortConnection2DataAccessSys10();

	/**
	 * Returns a new object of class '<em>mote Axiom r0</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>mote Axiom r0</em>'.
	 * @generated
	 */
	moteAxiom_r0 createmoteAxiom_r0();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access01 r401</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access01 r401</em>'.
	 * @generated
	 */
	PortConnection2DataAccess01_r401 createPortConnection2DataAccess01_r401();

	/**
	 * Returns a new object of class '<em>connection2connection r90</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connection2connection r90</em>'.
	 * @generated
	 */
	connection2connection_r90 createconnection2connection_r90();

	/**
	 * Returns a new object of class '<em>portconnection2portconnection4device Destination r90 DD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>portconnection2portconnection4device Destination r90 DD</em>'.
	 * @generated
	 */
	portconnection2portconnection4deviceDestination_r90DD createportconnection2portconnection4deviceDestination_r90DD();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access11 r411</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access11 r411</em>'.
	 * @generated
	 */
	PortConnection2DataAccess11_r411 createPortConnection2DataAccess11_r411();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys10 r810</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys10 r810</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys10_r810 createPortConnection2DataAccessSys10_r810();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys01 r801</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys01 r801</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys01_r801 createPortConnection2DataAccessSys01_r801();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device10 r61ffd10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device10 r61ffd10</em>'.
	 * @generated
	 */
	connectionref2connectionref4device10_r61ffd10 createconnectionref2connectionref4device10_r61ffd10();

	/**
	 * Returns a new object of class '<em>subcomponent2subcomponent r2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>subcomponent2subcomponent r2</em>'.
	 * @generated
	 */
	subcomponent2subcomponent_r2 createsubcomponent2subcomponent_r2();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access00 r400</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access00 r400</em>'.
	 * @generated
	 */
	PortConnection2DataAccess00_r400 createPortConnection2DataAccess00_r400();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys00 r800</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys00 r800</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys00_r800 createPortConnection2DataAccessSys00_r800();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref r61ff</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref r61ff</em>'.
	 * @generated
	 */
	connectionref2connectionref_r61ff createconnectionref2connectionref_r61ff();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device11 r61ffd11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device11 r61ffd11</em>'.
	 * @generated
	 */
	connectionref2connectionref4device11_r61ffd11 createconnectionref2connectionref4device11_r61ffd11();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access10 r410</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access10 r410</em>'.
	 * @generated
	 */
	PortConnection2DataAccess10_r410 createPortConnection2DataAccess10_r410();

	/**
	 * Returns a new object of class '<em>feature2feature r3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>feature2feature r3</em>'.
	 * @generated
	 */
	feature2feature_r3 createfeature2feature_r3();

	/**
	 * Returns a new object of class '<em>Port Connection2 Data Access Sys11 r811</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Connection2 Data Access Sys11 r811</em>'.
	 * @generated
	 */
	PortConnection2DataAccessSys11_r811 createPortConnection2DataAccessSys11_r811();

	/**
	 * Returns a new object of class '<em>connectionref2connectionref4device01 r61ffd01</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>connectionref2connectionref4device01 r61ffd01</em>'.
	 * @generated
	 */
	connectionref2connectionref4device01_r61ffd01 createconnectionref2connectionref4device01_r61ffd01();

	/**
	 * Returns a new object of class '<em>portconnection2portconnection4device Source r90 DS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>portconnection2portconnection4device Source r90 DS</em>'.
	 * @generated
	 */
	portconnection2portconnection4deviceSource_r90DS createportconnection2portconnection4deviceSource_r90DS();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GeneratedPackage getGeneratedPackage();

} //GeneratedFactory
