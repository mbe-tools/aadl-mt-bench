/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage#getCorrAxiom()
 * @model
 * @generated
 */
public interface CorrAxiom extends CorrComponent2Component {
} // CorrAxiom
