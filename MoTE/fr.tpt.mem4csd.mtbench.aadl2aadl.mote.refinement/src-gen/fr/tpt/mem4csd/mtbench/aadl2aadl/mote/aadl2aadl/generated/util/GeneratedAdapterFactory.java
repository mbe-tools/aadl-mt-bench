/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.util;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiom;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiomGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMapping;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMappingGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRule;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGG;

import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage
 * @generated
 */
public class GeneratedAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GeneratedPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = GeneratedPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeneratedSwitch<Adapter> modelSwitch = new GeneratedSwitch<Adapter>() {
		@Override
		public Adapter caseaadl2aadlOperationalTGG(aadl2aadlOperationalTGG object) {
			return createaadl2aadlOperationalTGGAdapter();
		}

		@Override
		public Adapter casemoteAxiom(moteAxiom object) {
			return createmoteAxiomAdapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device10(connectionref2connectionref4device10 object) {
			return createconnectionref2connectionref4device10Adapter();
		}

		@Override
		public Adapter caseportconnection2portconnection4deviceDestination(
				portconnection2portconnection4deviceDestination object) {
			return createportconnection2portconnection4deviceDestinationAdapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device01(connectionref2connectionref4device01 object) {
			return createconnectionref2connectionref4device01Adapter();
		}

		@Override
		public Adapter casefeature2feature(feature2feature object) {
			return createfeature2featureAdapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess01(PortConnection2DataAccess01 object) {
			return createPortConnection2DataAccess01Adapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref(connectionref2connectionref object) {
			return createconnectionref2connectionrefAdapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys01(PortConnection2DataAccessSys01 object) {
			return createPortConnection2DataAccessSys01Adapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device11(connectionref2connectionref4device11 object) {
			return createconnectionref2connectionref4device11Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys00(PortConnection2DataAccessSys00 object) {
			return createPortConnection2DataAccessSys00Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess11(PortConnection2DataAccess11 object) {
			return createPortConnection2DataAccess11Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess10(PortConnection2DataAccess10 object) {
			return createPortConnection2DataAccess10Adapter();
		}

		@Override
		public Adapter caseportconnection2portconnection4deviceSource(
				portconnection2portconnection4deviceSource object) {
			return createportconnection2portconnection4deviceSourceAdapter();
		}

		@Override
		public Adapter caseconnection2connection(connection2connection object) {
			return createconnection2connectionAdapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys11(PortConnection2DataAccessSys11 object) {
			return createPortConnection2DataAccessSys11Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess00(PortConnection2DataAccess00 object) {
			return createPortConnection2DataAccess00Adapter();
		}

		@Override
		public Adapter casesubcomponent2subcomponent(subcomponent2subcomponent object) {
			return createsubcomponent2subcomponentAdapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys10(PortConnection2DataAccessSys10 object) {
			return createPortConnection2DataAccessSys10Adapter();
		}

		@Override
		public Adapter casemoteAxiom_r0(moteAxiom_r0 object) {
			return createmoteAxiom_r0Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess01_r401(PortConnection2DataAccess01_r401 object) {
			return createPortConnection2DataAccess01_r401Adapter();
		}

		@Override
		public Adapter caseconnection2connection_r90(connection2connection_r90 object) {
			return createconnection2connection_r90Adapter();
		}

		@Override
		public Adapter caseportconnection2portconnection4deviceDestination_r90DD(
				portconnection2portconnection4deviceDestination_r90DD object) {
			return createportconnection2portconnection4deviceDestination_r90DDAdapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess11_r411(PortConnection2DataAccess11_r411 object) {
			return createPortConnection2DataAccess11_r411Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys10_r810(PortConnection2DataAccessSys10_r810 object) {
			return createPortConnection2DataAccessSys10_r810Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys01_r801(PortConnection2DataAccessSys01_r801 object) {
			return createPortConnection2DataAccessSys01_r801Adapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device10_r61ffd10(
				connectionref2connectionref4device10_r61ffd10 object) {
			return createconnectionref2connectionref4device10_r61ffd10Adapter();
		}

		@Override
		public Adapter casesubcomponent2subcomponent_r2(subcomponent2subcomponent_r2 object) {
			return createsubcomponent2subcomponent_r2Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess00_r400(PortConnection2DataAccess00_r400 object) {
			return createPortConnection2DataAccess00_r400Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys00_r800(PortConnection2DataAccessSys00_r800 object) {
			return createPortConnection2DataAccessSys00_r800Adapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref_r61ff(connectionref2connectionref_r61ff object) {
			return createconnectionref2connectionref_r61ffAdapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device11_r61ffd11(
				connectionref2connectionref4device11_r61ffd11 object) {
			return createconnectionref2connectionref4device11_r61ffd11Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccess10_r410(PortConnection2DataAccess10_r410 object) {
			return createPortConnection2DataAccess10_r410Adapter();
		}

		@Override
		public Adapter casefeature2feature_r3(feature2feature_r3 object) {
			return createfeature2feature_r3Adapter();
		}

		@Override
		public Adapter casePortConnection2DataAccessSys11_r811(PortConnection2DataAccessSys11_r811 object) {
			return createPortConnection2DataAccessSys11_r811Adapter();
		}

		@Override
		public Adapter caseconnectionref2connectionref4device01_r61ffd01(
				connectionref2connectionref4device01_r61ffd01 object) {
			return createconnectionref2connectionref4device01_r61ffd01Adapter();
		}

		@Override
		public Adapter caseportconnection2portconnection4deviceSource_r90DS(
				portconnection2portconnection4deviceSource_r90DS object) {
			return createportconnection2portconnection4deviceSource_r90DSAdapter();
		}

		@Override
		public Adapter caseOperationalTGG(OperationalTGG object) {
			return createOperationalTGGAdapter();
		}

		@Override
		public Adapter caseSdmOperationalTGG(SdmOperationalTGG object) {
			return createSdmOperationalTGGAdapter();
		}

		@Override
		public Adapter caseOperationalMappingGroup(OperationalMappingGroup object) {
			return createOperationalMappingGroupAdapter();
		}

		@Override
		public Adapter caseOperationalAxiomGroup(OperationalAxiomGroup object) {
			return createOperationalAxiomGroupAdapter();
		}

		@Override
		public Adapter caseOperationalRuleGroup(OperationalRuleGroup object) {
			return createOperationalRuleGroupAdapter();
		}

		@Override
		public Adapter caseOperationalMapping(OperationalMapping object) {
			return createOperationalMappingAdapter();
		}

		@Override
		public Adapter caseOperationalAxiom(OperationalAxiom object) {
			return createOperationalAxiomAdapter();
		}

		@Override
		public Adapter caseOperationalRule(OperationalRule object) {
			return createOperationalRuleAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.aadl2aadlOperationalTGG <em>aadl2aadl Operational TGG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.aadl2aadlOperationalTGG
	 * @generated
	 */
	public Adapter createaadl2aadlOperationalTGGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom <em>mote Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom
	 * @generated
	 */
	public Adapter createmoteAxiomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10 <em>connectionref2connectionref4device10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination <em>portconnection2portconnection4device Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination
	 * @generated
	 */
	public Adapter createportconnection2portconnection4deviceDestinationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01 <em>connectionref2connectionref4device01</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device01Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature <em>feature2feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature
	 * @generated
	 */
	public Adapter createfeature2featureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01 <em>Port Connection2 Data Access01</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess01Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref <em>connectionref2connectionref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref
	 * @generated
	 */
	public Adapter createconnectionref2connectionrefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01 <em>Port Connection2 Data Access Sys01</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys01Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11 <em>connectionref2connectionref4device11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00 <em>Port Connection2 Data Access Sys00</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys00Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11 <em>Port Connection2 Data Access11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10 <em>Port Connection2 Data Access10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource <em>portconnection2portconnection4device Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource
	 * @generated
	 */
	public Adapter createportconnection2portconnection4deviceSourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection <em>connection2connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection
	 * @generated
	 */
	public Adapter createconnection2connectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11 <em>Port Connection2 Data Access Sys11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00 <em>Port Connection2 Data Access00</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess00Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent <em>subcomponent2subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent
	 * @generated
	 */
	public Adapter createsubcomponent2subcomponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10 <em>Port Connection2 Data Access Sys10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0 <em>mote Axiom r0</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0
	 * @generated
	 */
	public Adapter createmoteAxiom_r0Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401 <em>Port Connection2 Data Access01 r401</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess01_r401Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90 <em>connection2connection r90</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90
	 * @generated
	 */
	public Adapter createconnection2connection_r90Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD <em>portconnection2portconnection4device Destination r90 DD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD
	 * @generated
	 */
	public Adapter createportconnection2portconnection4deviceDestination_r90DDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411 <em>Port Connection2 Data Access11 r411</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess11_r411Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810 <em>Port Connection2 Data Access Sys10 r810</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys10_r810Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801 <em>Port Connection2 Data Access Sys01 r801</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys01_r801Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10 <em>connectionref2connectionref4device10 r61ffd10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device10_r61ffd10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2 <em>subcomponent2subcomponent r2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2
	 * @generated
	 */
	public Adapter createsubcomponent2subcomponent_r2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400 <em>Port Connection2 Data Access00 r400</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess00_r400Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800 <em>Port Connection2 Data Access Sys00 r800</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys00_r800Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff <em>connectionref2connectionref r61ff</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff
	 * @generated
	 */
	public Adapter createconnectionref2connectionref_r61ffAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11 <em>connectionref2connectionref4device11 r61ffd11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device11_r61ffd11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410 <em>Port Connection2 Data Access10 r410</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410
	 * @generated
	 */
	public Adapter createPortConnection2DataAccess10_r410Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3 <em>feature2feature r3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3
	 * @generated
	 */
	public Adapter createfeature2feature_r3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811 <em>Port Connection2 Data Access Sys11 r811</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811
	 * @generated
	 */
	public Adapter createPortConnection2DataAccessSys11_r811Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01 <em>connectionref2connectionref4device01 r61ffd01</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01
	 * @generated
	 */
	public Adapter createconnectionref2connectionref4device01_r61ffd01Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS <em>portconnection2portconnection4device Source r90 DS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS
	 * @generated
	 */
	public Adapter createportconnection2portconnection4deviceSource_r90DSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalTGG <em>Operational TGG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalTGG
	 * @generated
	 */
	public Adapter createOperationalTGGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG <em>Operational TGG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG
	 * @generated
	 */
	public Adapter createSdmOperationalTGGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalMappingGroup <em>Operational Mapping Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalMappingGroup
	 * @generated
	 */
	public Adapter createOperationalMappingGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiomGroup <em>Operational Axiom Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiomGroup
	 * @generated
	 */
	public Adapter createOperationalAxiomGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup <em>Operational Rule Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup
	 * @generated
	 */
	public Adapter createOperationalRuleGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalMapping <em>Operational Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalMapping
	 * @generated
	 */
	public Adapter createOperationalMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiom <em>Operational Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiom
	 * @generated
	 */
	public Adapter createOperationalAxiomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.mdelab.mltgg.mote2.operationalTGG.OperationalRule <em>Operational Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.mdelab.mltgg.mote2.operationalTGG.OperationalRule
	 * @generated
	 */
	public Adapter createOperationalRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //GeneratedAdapterFactory
