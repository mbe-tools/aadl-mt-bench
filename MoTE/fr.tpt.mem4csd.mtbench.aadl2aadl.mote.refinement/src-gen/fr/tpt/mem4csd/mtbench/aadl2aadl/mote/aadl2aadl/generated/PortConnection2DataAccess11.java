/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connection2 Data Access11</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage#getPortConnection2DataAccess11()
 * @model
 * @generated
 */
public interface PortConnection2DataAccess11 extends OperationalRuleGroup {
} // PortConnection2DataAccess11
