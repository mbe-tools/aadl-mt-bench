/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.CorrConnection2DataAccessSys;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Connection2 Data Access Sys</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrConnection2DataAccessSysImpl extends CorrFeature2FeatureImpl implements CorrConnection2DataAccessSys {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrConnection2DataAccessSysImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Aadl2aadlPackage.Literals.CORR_CONNECTION2_DATA_ACCESS_SYS;
	}

} //CorrConnection2DataAccessSysImpl
