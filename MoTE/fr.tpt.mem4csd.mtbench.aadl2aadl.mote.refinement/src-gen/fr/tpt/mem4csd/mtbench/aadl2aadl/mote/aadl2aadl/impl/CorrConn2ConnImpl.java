/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.CorrConn2Conn;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Conn2 Conn</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrConn2ConnImpl extends CorrFeature2FeatureImpl implements CorrConn2Conn {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrConn2ConnImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Aadl2aadlPackage.Literals.CORR_CONN2_CONN;
	}

} //CorrConn2ConnImpl
