/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import de.mdelab.mltgg.mote2.operationalTGG.impl.OperationalRuleGroupImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>connectionref2connectionref4device10</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class connectionref2connectionref4device10Impl extends OperationalRuleGroupImpl
		implements connectionref2connectionref4device10 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected connectionref2connectionref4device10Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratedPackage.eINSTANCE.getconnectionref2connectionref4device10();
	}

} //connectionref2connectionref4device10Impl
