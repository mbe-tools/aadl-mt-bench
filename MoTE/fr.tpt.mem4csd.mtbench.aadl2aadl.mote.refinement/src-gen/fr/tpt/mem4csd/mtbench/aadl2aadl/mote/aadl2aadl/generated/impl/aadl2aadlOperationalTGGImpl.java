/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import de.mdelab.mltgg.mote2.sdm.impl.SdmOperationalTGGImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.aadl2aadlOperationalTGG;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>aadl2aadl Operational TGG</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class aadl2aadlOperationalTGGImpl extends SdmOperationalTGGImpl implements aadl2aadlOperationalTGG {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected aadl2aadlOperationalTGGImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratedPackage.eINSTANCE.getaadl2aadlOperationalTGG();
	}

} //aadl2aadlOperationalTGGImpl
