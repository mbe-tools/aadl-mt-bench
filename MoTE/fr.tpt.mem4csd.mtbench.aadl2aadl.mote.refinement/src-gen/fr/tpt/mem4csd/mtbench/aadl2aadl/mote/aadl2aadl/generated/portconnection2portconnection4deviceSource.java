/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>portconnection2portconnection4device Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage#getportconnection2portconnection4deviceSource()
 * @model
 * @generated
 */
public interface portconnection2portconnection4deviceSource extends OperationalRuleGroup {
} // portconnection2portconnection4deviceSource
