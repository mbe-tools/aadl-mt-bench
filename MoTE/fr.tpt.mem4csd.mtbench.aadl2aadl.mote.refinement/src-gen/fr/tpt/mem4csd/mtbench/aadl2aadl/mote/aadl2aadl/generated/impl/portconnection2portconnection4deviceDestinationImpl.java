/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import de.mdelab.mltgg.mote2.operationalTGG.impl.OperationalRuleGroupImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>portconnection2portconnection4device Destination</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class portconnection2portconnection4deviceDestinationImpl extends OperationalRuleGroupImpl
		implements portconnection2portconnection4deviceDestination {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected portconnection2portconnection4deviceDestinationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratedPackage.eINSTANCE.getportconnection2portconnection4deviceDestination();
	}

} //portconnection2portconnection4deviceDestinationImpl
