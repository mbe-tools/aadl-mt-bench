/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GeneratedFactoryImpl extends EFactoryImpl implements GeneratedFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GeneratedFactory init() {
		try {
			GeneratedFactory theGeneratedFactory = (GeneratedFactory) EPackage.Registry.INSTANCE
					.getEFactory(GeneratedPackage.eNS_URI);
			if (theGeneratedFactory != null) {
				return theGeneratedFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GeneratedFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case GeneratedPackage.AADL2AADL_OPERATIONAL_TGG:
			return createaadl2aadlOperationalTGG();
		case GeneratedPackage.MOTE_AXIOM:
			return createmoteAxiom();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE10:
			return createconnectionref2connectionref4device10();
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION:
			return createportconnection2portconnection4deviceDestination();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE01:
			return createconnectionref2connectionref4device01();
		case GeneratedPackage.FEATURE2FEATURE:
			return createfeature2feature();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS01:
			return createPortConnection2DataAccess01();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF:
			return createconnectionref2connectionref();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS01:
			return createPortConnection2DataAccessSys01();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE11:
			return createconnectionref2connectionref4device11();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS00:
			return createPortConnection2DataAccessSys00();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS11:
			return createPortConnection2DataAccess11();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS10:
			return createPortConnection2DataAccess10();
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE:
			return createportconnection2portconnection4deviceSource();
		case GeneratedPackage.CONNECTION2CONNECTION:
			return createconnection2connection();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS11:
			return createPortConnection2DataAccessSys11();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS00:
			return createPortConnection2DataAccess00();
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT:
			return createsubcomponent2subcomponent();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS10:
			return createPortConnection2DataAccessSys10();
		case GeneratedPackage.MOTE_AXIOM_R0:
			return createmoteAxiom_r0();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS01_R401:
			return createPortConnection2DataAccess01_r401();
		case GeneratedPackage.CONNECTION2CONNECTION_R90:
			return createconnection2connection_r90();
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD:
			return createportconnection2portconnection4deviceDestination_r90DD();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS11_R411:
			return createPortConnection2DataAccess11_r411();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS10_R810:
			return createPortConnection2DataAccessSys10_r810();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS01_R801:
			return createPortConnection2DataAccessSys01_r801();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10:
			return createconnectionref2connectionref4device10_r61ffd10();
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT_R2:
			return createsubcomponent2subcomponent_r2();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS00_R400:
			return createPortConnection2DataAccess00_r400();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS00_R800:
			return createPortConnection2DataAccessSys00_r800();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF_R61FF:
			return createconnectionref2connectionref_r61ff();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11:
			return createconnectionref2connectionref4device11_r61ffd11();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS10_R410:
			return createPortConnection2DataAccess10_r410();
		case GeneratedPackage.FEATURE2FEATURE_R3:
			return createfeature2feature_r3();
		case GeneratedPackage.PORT_CONNECTION2_DATA_ACCESS_SYS11_R811:
			return createPortConnection2DataAccessSys11_r811();
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01:
			return createconnectionref2connectionref4device01_r61ffd01();
		case GeneratedPackage.PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS:
			return createportconnection2portconnection4deviceSource_r90DS();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public aadl2aadlOperationalTGG createaadl2aadlOperationalTGG() {
		aadl2aadlOperationalTGGImpl aadl2aadlOperationalTGG = new aadl2aadlOperationalTGGImpl();
		return aadl2aadlOperationalTGG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public moteAxiom createmoteAxiom() {
		moteAxiomImpl moteAxiom = new moteAxiomImpl();
		return moteAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device10 createconnectionref2connectionref4device10() {
		connectionref2connectionref4device10Impl connectionref2connectionref4device10 = new connectionref2connectionref4device10Impl();
		return connectionref2connectionref4device10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public portconnection2portconnection4deviceDestination createportconnection2portconnection4deviceDestination() {
		portconnection2portconnection4deviceDestinationImpl portconnection2portconnection4deviceDestination = new portconnection2portconnection4deviceDestinationImpl();
		return portconnection2portconnection4deviceDestination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device01 createconnectionref2connectionref4device01() {
		connectionref2connectionref4device01Impl connectionref2connectionref4device01 = new connectionref2connectionref4device01Impl();
		return connectionref2connectionref4device01;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public feature2feature createfeature2feature() {
		feature2featureImpl feature2feature = new feature2featureImpl();
		return feature2feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess01 createPortConnection2DataAccess01() {
		PortConnection2DataAccess01Impl portConnection2DataAccess01 = new PortConnection2DataAccess01Impl();
		return portConnection2DataAccess01;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref createconnectionref2connectionref() {
		connectionref2connectionrefImpl connectionref2connectionref = new connectionref2connectionrefImpl();
		return connectionref2connectionref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys01 createPortConnection2DataAccessSys01() {
		PortConnection2DataAccessSys01Impl portConnection2DataAccessSys01 = new PortConnection2DataAccessSys01Impl();
		return portConnection2DataAccessSys01;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device11 createconnectionref2connectionref4device11() {
		connectionref2connectionref4device11Impl connectionref2connectionref4device11 = new connectionref2connectionref4device11Impl();
		return connectionref2connectionref4device11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys00 createPortConnection2DataAccessSys00() {
		PortConnection2DataAccessSys00Impl portConnection2DataAccessSys00 = new PortConnection2DataAccessSys00Impl();
		return portConnection2DataAccessSys00;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess11 createPortConnection2DataAccess11() {
		PortConnection2DataAccess11Impl portConnection2DataAccess11 = new PortConnection2DataAccess11Impl();
		return portConnection2DataAccess11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess10 createPortConnection2DataAccess10() {
		PortConnection2DataAccess10Impl portConnection2DataAccess10 = new PortConnection2DataAccess10Impl();
		return portConnection2DataAccess10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public portconnection2portconnection4deviceSource createportconnection2portconnection4deviceSource() {
		portconnection2portconnection4deviceSourceImpl portconnection2portconnection4deviceSource = new portconnection2portconnection4deviceSourceImpl();
		return portconnection2portconnection4deviceSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connection2connection createconnection2connection() {
		connection2connectionImpl connection2connection = new connection2connectionImpl();
		return connection2connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys11 createPortConnection2DataAccessSys11() {
		PortConnection2DataAccessSys11Impl portConnection2DataAccessSys11 = new PortConnection2DataAccessSys11Impl();
		return portConnection2DataAccessSys11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess00 createPortConnection2DataAccess00() {
		PortConnection2DataAccess00Impl portConnection2DataAccess00 = new PortConnection2DataAccess00Impl();
		return portConnection2DataAccess00;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public subcomponent2subcomponent createsubcomponent2subcomponent() {
		subcomponent2subcomponentImpl subcomponent2subcomponent = new subcomponent2subcomponentImpl();
		return subcomponent2subcomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys10 createPortConnection2DataAccessSys10() {
		PortConnection2DataAccessSys10Impl portConnection2DataAccessSys10 = new PortConnection2DataAccessSys10Impl();
		return portConnection2DataAccessSys10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public moteAxiom_r0 createmoteAxiom_r0() {
		moteAxiom_r0Impl moteAxiom_r0 = new moteAxiom_r0Impl();
		return moteAxiom_r0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess01_r401 createPortConnection2DataAccess01_r401() {
		PortConnection2DataAccess01_r401Impl portConnection2DataAccess01_r401 = new PortConnection2DataAccess01_r401Impl();
		return portConnection2DataAccess01_r401;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connection2connection_r90 createconnection2connection_r90() {
		connection2connection_r90Impl connection2connection_r90 = new connection2connection_r90Impl();
		return connection2connection_r90;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public portconnection2portconnection4deviceDestination_r90DD createportconnection2portconnection4deviceDestination_r90DD() {
		portconnection2portconnection4deviceDestination_r90DDImpl portconnection2portconnection4deviceDestination_r90DD = new portconnection2portconnection4deviceDestination_r90DDImpl();
		return portconnection2portconnection4deviceDestination_r90DD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess11_r411 createPortConnection2DataAccess11_r411() {
		PortConnection2DataAccess11_r411Impl portConnection2DataAccess11_r411 = new PortConnection2DataAccess11_r411Impl();
		return portConnection2DataAccess11_r411;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys10_r810 createPortConnection2DataAccessSys10_r810() {
		PortConnection2DataAccessSys10_r810Impl portConnection2DataAccessSys10_r810 = new PortConnection2DataAccessSys10_r810Impl();
		return portConnection2DataAccessSys10_r810;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys01_r801 createPortConnection2DataAccessSys01_r801() {
		PortConnection2DataAccessSys01_r801Impl portConnection2DataAccessSys01_r801 = new PortConnection2DataAccessSys01_r801Impl();
		return portConnection2DataAccessSys01_r801;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device10_r61ffd10 createconnectionref2connectionref4device10_r61ffd10() {
		connectionref2connectionref4device10_r61ffd10Impl connectionref2connectionref4device10_r61ffd10 = new connectionref2connectionref4device10_r61ffd10Impl();
		return connectionref2connectionref4device10_r61ffd10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public subcomponent2subcomponent_r2 createsubcomponent2subcomponent_r2() {
		subcomponent2subcomponent_r2Impl subcomponent2subcomponent_r2 = new subcomponent2subcomponent_r2Impl();
		return subcomponent2subcomponent_r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess00_r400 createPortConnection2DataAccess00_r400() {
		PortConnection2DataAccess00_r400Impl portConnection2DataAccess00_r400 = new PortConnection2DataAccess00_r400Impl();
		return portConnection2DataAccess00_r400;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys00_r800 createPortConnection2DataAccessSys00_r800() {
		PortConnection2DataAccessSys00_r800Impl portConnection2DataAccessSys00_r800 = new PortConnection2DataAccessSys00_r800Impl();
		return portConnection2DataAccessSys00_r800;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref_r61ff createconnectionref2connectionref_r61ff() {
		connectionref2connectionref_r61ffImpl connectionref2connectionref_r61ff = new connectionref2connectionref_r61ffImpl();
		return connectionref2connectionref_r61ff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device11_r61ffd11 createconnectionref2connectionref4device11_r61ffd11() {
		connectionref2connectionref4device11_r61ffd11Impl connectionref2connectionref4device11_r61ffd11 = new connectionref2connectionref4device11_r61ffd11Impl();
		return connectionref2connectionref4device11_r61ffd11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccess10_r410 createPortConnection2DataAccess10_r410() {
		PortConnection2DataAccess10_r410Impl portConnection2DataAccess10_r410 = new PortConnection2DataAccess10_r410Impl();
		return portConnection2DataAccess10_r410;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public feature2feature_r3 createfeature2feature_r3() {
		feature2feature_r3Impl feature2feature_r3 = new feature2feature_r3Impl();
		return feature2feature_r3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnection2DataAccessSys11_r811 createPortConnection2DataAccessSys11_r811() {
		PortConnection2DataAccessSys11_r811Impl portConnection2DataAccessSys11_r811 = new PortConnection2DataAccessSys11_r811Impl();
		return portConnection2DataAccessSys11_r811;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public connectionref2connectionref4device01_r61ffd01 createconnectionref2connectionref4device01_r61ffd01() {
		connectionref2connectionref4device01_r61ffd01Impl connectionref2connectionref4device01_r61ffd01 = new connectionref2connectionref4device01_r61ffd01Impl();
		return connectionref2connectionref4device01_r61ffd01;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public portconnection2portconnection4deviceSource_r90DS createportconnection2portconnection4deviceSource_r90DS() {
		portconnection2portconnection4deviceSource_r90DSImpl portconnection2portconnection4deviceSource_r90DS = new portconnection2portconnection4deviceSource_r90DSImpl();
		return portconnection2portconnection4deviceSource_r90DS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedPackage getGeneratedPackage() {
		return (GeneratedPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GeneratedPackage getPackage() {
		return GeneratedPackage.eINSTANCE;
	}

} //GeneratedFactoryImpl
