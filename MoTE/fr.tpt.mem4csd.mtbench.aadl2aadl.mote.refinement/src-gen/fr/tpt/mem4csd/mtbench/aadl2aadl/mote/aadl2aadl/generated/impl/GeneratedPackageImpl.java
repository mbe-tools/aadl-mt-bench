/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import de.mdelab.mlcallactions.MlcallactionsPackage;

import de.mdelab.mlcore.MlcorePackage;

import de.mdelab.mlexpressions.MlexpressionsPackage;

import de.mdelab.mlsdm.MlsdmPackage;

import de.mdelab.mlstorypatterns.MlstorypatternsPackage;

import de.mdelab.mltgg.mote2.Mote2Package;

import de.mdelab.mltgg.mote2.sdm.SdmPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedFactory;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.impl.Aadl2aadlPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GeneratedPackageImpl extends EPackageImpl implements GeneratedPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadl2aadlOperationalTGGEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteAxiomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device10EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portconnection2portconnection4deviceDestinationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device01EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature2featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess01EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys01EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device11EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys00EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess11EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess10EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portconnection2portconnection4deviceSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connection2connectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys11EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess00EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subcomponent2subcomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys10EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteAxiom_r0EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess01_r401EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connection2connection_r90EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portconnection2portconnection4deviceDestination_r90DDEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess11_r411EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys10_r810EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys01_r801EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device10_r61ffd10EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subcomponent2subcomponent_r2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess00_r400EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys00_r800EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref_r61ffEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device11_r61ffd11EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccess10_r410EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature2feature_r3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnection2DataAccessSys11_r811EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref4device01_r61ffd01EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portconnection2portconnection4deviceSource_r90DSEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GeneratedPackageImpl() {
		super(eNS_URI, GeneratedFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link GeneratedPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static GeneratedPackage init() {
		if (isInited)
			return (GeneratedPackage) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredGeneratedPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		GeneratedPackageImpl theGeneratedPackage = registeredGeneratedPackage instanceof GeneratedPackageImpl
				? (GeneratedPackageImpl) registeredGeneratedPackage
				: new GeneratedPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Mote2Package.eINSTANCE.eClass();
		MlcorePackage.eINSTANCE.eClass();
		SdmPackage.eINSTANCE.eClass();
		MlsdmPackage.eINSTANCE.eClass();
		MlexpressionsPackage.eINSTANCE.eClass();
		MlstorypatternsPackage.eINSTANCE.eClass();
		MlcallactionsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(Aadl2aadlPackage.eNS_URI);
		Aadl2aadlPackageImpl theAadl2aadlPackage = (Aadl2aadlPackageImpl) (registeredPackage instanceof Aadl2aadlPackageImpl
				? registeredPackage
				: Aadl2aadlPackage.eINSTANCE);

		// Load packages
		theAadl2aadlPackage.loadPackage();

		// Fix loaded packages
		theGeneratedPackage.fixPackageContents();
		theAadl2aadlPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theGeneratedPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GeneratedPackage.eNS_URI, theGeneratedPackage);
		return theGeneratedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getaadl2aadlOperationalTGG() {
		if (aadl2aadlOperationalTGGEClass == null) {
			aadl2aadlOperationalTGGEClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(0);
		}
		return aadl2aadlOperationalTGGEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getmoteAxiom() {
		if (moteAxiomEClass == null) {
			moteAxiomEClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(1);
		}
		return moteAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device10() {
		if (connectionref2connectionref4device10EClass == null) {
			connectionref2connectionref4device10EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(2);
		}
		return connectionref2connectionref4device10EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getportconnection2portconnection4deviceDestination() {
		if (portconnection2portconnection4deviceDestinationEClass == null) {
			portconnection2portconnection4deviceDestinationEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(3);
		}
		return portconnection2portconnection4deviceDestinationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device01() {
		if (connectionref2connectionref4device01EClass == null) {
			connectionref2connectionref4device01EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(4);
		}
		return connectionref2connectionref4device01EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfeature2feature() {
		if (feature2featureEClass == null) {
			feature2featureEClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(5);
		}
		return feature2featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess01() {
		if (portConnection2DataAccess01EClass == null) {
			portConnection2DataAccess01EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(6);
		}
		return portConnection2DataAccess01EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref() {
		if (connectionref2connectionrefEClass == null) {
			connectionref2connectionrefEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(7);
		}
		return connectionref2connectionrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys01() {
		if (portConnection2DataAccessSys01EClass == null) {
			portConnection2DataAccessSys01EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(8);
		}
		return portConnection2DataAccessSys01EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device11() {
		if (connectionref2connectionref4device11EClass == null) {
			connectionref2connectionref4device11EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(9);
		}
		return connectionref2connectionref4device11EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys00() {
		if (portConnection2DataAccessSys00EClass == null) {
			portConnection2DataAccessSys00EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(10);
		}
		return portConnection2DataAccessSys00EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess11() {
		if (portConnection2DataAccess11EClass == null) {
			portConnection2DataAccess11EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(11);
		}
		return portConnection2DataAccess11EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess10() {
		if (portConnection2DataAccess10EClass == null) {
			portConnection2DataAccess10EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(12);
		}
		return portConnection2DataAccess10EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getportconnection2portconnection4deviceSource() {
		if (portconnection2portconnection4deviceSourceEClass == null) {
			portconnection2portconnection4deviceSourceEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(13);
		}
		return portconnection2portconnection4deviceSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnection2connection() {
		if (connection2connectionEClass == null) {
			connection2connectionEClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(14);
		}
		return connection2connectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys11() {
		if (portConnection2DataAccessSys11EClass == null) {
			portConnection2DataAccessSys11EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(15);
		}
		return portConnection2DataAccessSys11EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess00() {
		if (portConnection2DataAccess00EClass == null) {
			portConnection2DataAccess00EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(16);
		}
		return portConnection2DataAccess00EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsubcomponent2subcomponent() {
		if (subcomponent2subcomponentEClass == null) {
			subcomponent2subcomponentEClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(17);
		}
		return subcomponent2subcomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys10() {
		if (portConnection2DataAccessSys10EClass == null) {
			portConnection2DataAccessSys10EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(18);
		}
		return portConnection2DataAccessSys10EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getmoteAxiom_r0() {
		if (moteAxiom_r0EClass == null) {
			moteAxiom_r0EClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(19);
		}
		return moteAxiom_r0EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_AddElementActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_MoveElementActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_ChangeAttributeActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformForwardActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformMappingActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformBackwardActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_SynchronizeForwardActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_SynchronizeBackwardActivity() {
		return (EReference) getmoteAxiom_r0().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__AddElement__EMap_EList_EList() {
		return getmoteAxiom_r0().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap() {
		return getmoteAxiom_r0().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformForward__EList_EList_boolean() {
		return getmoteAxiom_r0().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformMapping__EList_EList_boolean() {
		return getmoteAxiom_r0().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformBackward__EList_EList_boolean() {
		return getmoteAxiom_r0().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean() {
		return getmoteAxiom_r0().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean() {
		return getmoteAxiom_r0().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess01_r401() {
		if (portConnection2DataAccess01_r401EClass == null) {
			portConnection2DataAccess01_r401EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(20);
		}
		return portConnection2DataAccess01_r401EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_AddElementActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess01_r401_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccess01_r401().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__AddElement__EMap() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess01_r401__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess01_r401().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnection2connection_r90() {
		if (connection2connection_r90EClass == null) {
			connection2connection_r90EClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(21);
		}
		return connection2connection_r90EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_AddElementActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_MoveElementActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_ChangeAttributeActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_TransformForwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_TransformMappingActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_TransformBackwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_ConflictCheckForwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_ConflictCheckMappingActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_ConflictCheckBackwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_SynchronizeForwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_SynchronizeBackwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_RepairForwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90_RepairBackwardActivity() {
		return (EReference) getconnection2connection_r90().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__AddElement__EMap() {
		return getconnection2connection_r90().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__ChangeAttributeValues__TGGNode_EMap() {
		return getconnection2connection_r90().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getconnection2connection_r90().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__TransformForward__TGGNode_boolean_boolean() {
		return getconnection2connection_r90().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__TransformMapping__TGGNode_boolean_boolean() {
		return getconnection2connection_r90().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__TransformBackward__TGGNode_boolean_boolean() {
		return getconnection2connection_r90().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__ConflictCheckForward__TGGNode() {
		return getconnection2connection_r90().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__ConflictCheckMapping__TGGNode() {
		return getconnection2connection_r90().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__ConflictCheckBackward__TGGNode() {
		return getconnection2connection_r90().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__SynchronizeForward__TGGNode_boolean() {
		return getconnection2connection_r90().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__SynchronizeBackward__TGGNode_boolean() {
		return getconnection2connection_r90().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__RepairForward__TGGNode_boolean() {
		return getconnection2connection_r90().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90__RepairBackward__TGGNode_boolean() {
		return getconnection2connection_r90().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getportconnection2portconnection4deviceDestination_r90DD() {
		if (portconnection2portconnection4deviceDestination_r90DDEClass == null) {
			portconnection2portconnection4deviceDestination_r90DDEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(22);
		}
		return portconnection2portconnection4deviceDestination_r90DDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_AddElementActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_MoveElementActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_ChangeAttributeActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_TransformForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_TransformMappingActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_TransformBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckMappingActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_SynchronizeForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_SynchronizeBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_RepairForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceDestination_r90DD_RepairBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceDestination_r90DD().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__AddElement__EMap() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__ChangeAttributeValues__TGGNode_EMap() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformForward__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformMapping__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformBackward__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckForward__TGGNode() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckMapping__TGGNode() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckBackward__TGGNode() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__SynchronizeForward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__SynchronizeBackward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__RepairForward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceDestination_r90DD__RepairBackward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceDestination_r90DD().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess11_r411() {
		if (portConnection2DataAccess11_r411EClass == null) {
			portConnection2DataAccess11_r411EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(23);
		}
		return portConnection2DataAccess11_r411EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_AddElementActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess11_r411_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccess11_r411().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__AddElement__EMap() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess11_r411__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess11_r411().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys10_r810() {
		if (portConnection2DataAccessSys10_r810EClass == null) {
			portConnection2DataAccessSys10_r810EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(24);
		}
		return portConnection2DataAccessSys10_r810EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_AddElementActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys10_r810_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys10_r810().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__AddElement__EMap() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys10_r810__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys10_r810().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys01_r801() {
		if (portConnection2DataAccessSys01_r801EClass == null) {
			portConnection2DataAccessSys01_r801EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(25);
		}
		return portConnection2DataAccessSys01_r801EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_AddElementActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys01_r801_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys01_r801().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__AddElement__EMap() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys01_r801__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys01_r801().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device10_r61ffd10() {
		if (connectionref2connectionref4device10_r61ffd10EClass == null) {
			connectionref2connectionref4device10_r61ffd10EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(26);
		}
		return connectionref2connectionref4device10_r61ffd10EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_AddElementActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_MoveElementActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_ChangeAttributeActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_TransformForwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_TransformMappingActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_TransformBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckForwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckMappingActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_SynchronizeForwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_SynchronizeBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_RepairForwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device10_r61ffd10_RepairBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device10_r61ffd10().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__AddElement__EMap() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__ChangeAttributeValues__TGGNode_EMap() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__TransformForward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__TransformMapping__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__TransformBackward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckForward__TGGNode() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckMapping__TGGNode() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckBackward__TGGNode() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__SynchronizeForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__SynchronizeBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__RepairForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device10_r61ffd10__RepairBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device10_r61ffd10().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsubcomponent2subcomponent_r2() {
		if (subcomponent2subcomponent_r2EClass == null) {
			subcomponent2subcomponent_r2EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(27);
		}
		return subcomponent2subcomponent_r2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_AddElementActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_MoveElementActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ChangeAttributeActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformForwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformMappingActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformBackwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_SynchronizeForwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_RepairForwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_RepairBackwardActivity() {
		return (EReference) getsubcomponent2subcomponent_r2().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__AddElement__EMap() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean() {
		return getsubcomponent2subcomponent_r2().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess00_r400() {
		if (portConnection2DataAccess00_r400EClass == null) {
			portConnection2DataAccess00_r400EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(28);
		}
		return portConnection2DataAccess00_r400EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_AddElementActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess00_r400_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccess00_r400().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__AddElement__EMap() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess00_r400__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess00_r400().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys00_r800() {
		if (portConnection2DataAccessSys00_r800EClass == null) {
			portConnection2DataAccessSys00_r800EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(29);
		}
		return portConnection2DataAccessSys00_r800EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_AddElementActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys00_r800_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys00_r800().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__AddElement__EMap() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys00_r800__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys00_r800().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref_r61ff() {
		if (connectionref2connectionref_r61ffEClass == null) {
			connectionref2connectionref_r61ffEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(30);
		}
		return connectionref2connectionref_r61ffEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_AddElementActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_MoveElementActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_ChangeAttributeActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_TransformForwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_TransformMappingActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_TransformBackwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_ConflictCheckForwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_ConflictCheckMappingActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_ConflictCheckBackwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_SynchronizeForwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_SynchronizeBackwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_RepairForwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r61ff_RepairBackwardActivity() {
		return (EReference) getconnectionref2connectionref_r61ff().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__AddElement__EMap() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__ChangeAttributeValues__TGGNode_EMap() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__TransformForward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__TransformMapping__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__TransformBackward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__ConflictCheckForward__TGGNode() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__ConflictCheckMapping__TGGNode() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__ConflictCheckBackward__TGGNode() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__SynchronizeForward__TGGNode_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__SynchronizeBackward__TGGNode_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__RepairForward__TGGNode_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r61ff__RepairBackward__TGGNode_boolean() {
		return getconnectionref2connectionref_r61ff().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device11_r61ffd11() {
		if (connectionref2connectionref4device11_r61ffd11EClass == null) {
			connectionref2connectionref4device11_r61ffd11EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(31);
		}
		return connectionref2connectionref4device11_r61ffd11EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_AddElementActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_MoveElementActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_ChangeAttributeActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_TransformForwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_TransformMappingActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_TransformBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckForwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckMappingActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_SynchronizeForwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_SynchronizeBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_RepairForwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device11_r61ffd11_RepairBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device11_r61ffd11().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__AddElement__EMap() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__ChangeAttributeValues__TGGNode_EMap() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__TransformForward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__TransformMapping__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__TransformBackward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckForward__TGGNode() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckMapping__TGGNode() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckBackward__TGGNode() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__SynchronizeForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__SynchronizeBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__RepairForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device11_r61ffd11__RepairBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device11_r61ffd11().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccess10_r410() {
		if (portConnection2DataAccess10_r410EClass == null) {
			portConnection2DataAccess10_r410EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(32);
		}
		return portConnection2DataAccess10_r410EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_AddElementActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccess10_r410_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccess10_r410().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__AddElement__EMap() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccess10_r410__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccess10_r410().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfeature2feature_r3() {
		if (feature2feature_r3EClass == null) {
			feature2feature_r3EClass = (EClass) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI)
					.getEClassifiers().get(33);
		}
		return feature2feature_r3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_AddElementActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_MoveElementActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ChangeAttributeActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformForwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformMappingActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformBackwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckForwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckMappingActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckBackwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_SynchronizeForwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_SynchronizeBackwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_RepairForwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_RepairBackwardActivity() {
		return (EReference) getfeature2feature_r3().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__AddElement__EMap() {
		return getfeature2feature_r3().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap() {
		return getfeature2feature_r3().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getfeature2feature_r3().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean() {
		return getfeature2feature_r3().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean() {
		return getfeature2feature_r3().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean() {
		return getfeature2feature_r3().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckForward__TGGNode() {
		return getfeature2feature_r3().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckMapping__TGGNode() {
		return getfeature2feature_r3().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckBackward__TGGNode() {
		return getfeature2feature_r3().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__SynchronizeForward__TGGNode_boolean() {
		return getfeature2feature_r3().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean() {
		return getfeature2feature_r3().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__RepairForward__TGGNode_boolean() {
		return getfeature2feature_r3().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__RepairBackward__TGGNode_boolean() {
		return getfeature2feature_r3().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnection2DataAccessSys11_r811() {
		if (portConnection2DataAccessSys11_r811EClass == null) {
			portConnection2DataAccessSys11_r811EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(34);
		}
		return portConnection2DataAccessSys11_r811EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_AddElementActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_MoveElementActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_ChangeAttributeActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_TransformForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_TransformMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_TransformBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_ConflictCheckForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_ConflictCheckMappingActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_ConflictCheckBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_SynchronizeForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_SynchronizeBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_RepairForwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnection2DataAccessSys11_r811_RepairBackwardActivity() {
		return (EReference) getPortConnection2DataAccessSys11_r811().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__AddElement__EMap() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__ChangeAttributeValues__TGGNode_EMap() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__TransformForward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__TransformMapping__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__TransformBackward__TGGNode_boolean_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckForward__TGGNode() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckMapping__TGGNode() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckBackward__TGGNode() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__SynchronizeForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__SynchronizeBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__RepairForward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPortConnection2DataAccessSys11_r811__RepairBackward__TGGNode_boolean() {
		return getPortConnection2DataAccessSys11_r811().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref4device01_r61ffd01() {
		if (connectionref2connectionref4device01_r61ffd01EClass == null) {
			connectionref2connectionref4device01_r61ffd01EClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(35);
		}
		return connectionref2connectionref4device01_r61ffd01EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_AddElementActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_MoveElementActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_ChangeAttributeActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_TransformForwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_TransformMappingActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_TransformBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckForwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckMappingActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_SynchronizeForwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_SynchronizeBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_RepairForwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref4device01_r61ffd01_RepairBackwardActivity() {
		return (EReference) getconnectionref2connectionref4device01_r61ffd01().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__AddElement__EMap() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__ChangeAttributeValues__TGGNode_EMap() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__TransformForward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__TransformMapping__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__TransformBackward__TGGNode_boolean_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckForward__TGGNode() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckMapping__TGGNode() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckBackward__TGGNode() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__SynchronizeForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__SynchronizeBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__RepairForward__TGGNode_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref4device01_r61ffd01__RepairBackward__TGGNode_boolean() {
		return getconnectionref2connectionref4device01_r61ffd01().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getportconnection2portconnection4deviceSource_r90DS() {
		if (portconnection2portconnection4deviceSource_r90DSEClass == null) {
			portconnection2portconnection4deviceSource_r90DSEClass = (EClass) EPackage.Registry.INSTANCE
					.getEPackage(GeneratedPackage.eNS_URI).getEClassifiers().get(36);
		}
		return portconnection2portconnection4deviceSource_r90DSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_AddElementActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_MoveElementActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_ChangeAttributeActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_TransformForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_TransformMappingActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_TransformBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckMappingActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_SynchronizeForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_SynchronizeBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_RepairForwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getportconnection2portconnection4deviceSource_r90DS_RepairBackwardActivity() {
		return (EReference) getportconnection2portconnection4deviceSource_r90DS().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__AddElement__EMap() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__ChangeAttributeValues__TGGNode_EMap() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__MoveElement__TGGNode_TGGNode_TGGNode() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__TransformForward__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__TransformMapping__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__TransformBackward__TGGNode_boolean_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckForward__TGGNode() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckMapping__TGGNode() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckBackward__TGGNode() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__SynchronizeForward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__SynchronizeBackward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__RepairForward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getportconnection2portconnection4deviceSource_r90DS__RepairBackward__TGGNode_boolean() {
		return getportconnection2portconnection4deviceSource_r90DS().getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedFactory getGeneratedFactory() {
		return (GeneratedFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed)
			return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName(
					"fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //GeneratedPackageImpl
