package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.MScheduler.MSchedulerFactory
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.evm.specific.Lifecycles
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver
import org.eclipse.viatra.transformation.runtime.emf.rules.EventDrivenTransformationRuleGroup
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation.EventDrivenTransformationBuilder

class CopyingAadl2aadlTransformation extends AbstractTransformation {

	extension EventDrivenTransformationRuleFactory ruleFactory = new EventDrivenTransformationRuleFactory

	/** VIATRA Query Pattern group **/
	extension Copying_Required_queries required_queries = Copying_Required_queries.instance

	def getRules() {
		new EventDrivenTransformationRuleGroup(
			system2system,
			component2component,
			feature2feature,
			connection2connection,
			connectionRef2connectionRef
		)
	}

	override EventDrivenTransformationBuilder createTransformation(ViatraQueryEngine engine) {
		val fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver
		fixedPriorityResolver.setPriority(system2system.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(component2component.ruleSpecification, 50)

		fixedPriorityResolver.setPriority(feature2feature.ruleSpecification, 200)
		fixedPriorityResolver.setPriority(connection2connection.ruleSpecification, 300)
		fixedPriorityResolver.setPriority(connectionRef2connectionRef.ruleSpecification, 300)

		val builder = EventDrivenTransformation.forEngine(engine).setConflictResolver(fixedPriorityResolver).
			addRules(rules)
		builder.schedulerFactory = new MSchedulerFactory()

		return builder;

	}

	val system2system = createRule(Copy_system.instance).action(CRUDActivationStateEnum.CREATED) [
		// println("system2system " + system)
		systemRef.set(namedElement_Name, system.name)
		systemRef.set(componentInstance_Category, system.category)

		val source_system = system
		val target_system = systemRef

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, source_system)
			addTo(aadl2AadlTrace_RightInstance, target_system)
		]

	].action(CRUDActivationStateEnum.UPDATED)[].action(CRUDActivationStateEnum.DELETED)[].addLifeCycle(
		Lifecycles.getDefault(true, true)).build

	val component2component = createRule(Copy_component.instance).action(CRUDActivationStateEnum.CREATED) [
		val leftsubcomponent = subcomponent

		val rightsubcomponent = componentref.createChild(componentInstance_ComponentInstance, componentInstance)
		rightsubcomponent.set(namedElement_Name, leftsubcomponent.name)
		rightsubcomponent.set(componentInstance_Category, leftsubcomponent.category)

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, leftsubcomponent)
			addTo(aadl2AadlTrace_RightInstance, rightsubcomponent)
		]
	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, subcomponent, null).get

		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, subcomponent, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val feature2feature = createRule(Copy_feature.instance).action(CRUDActivationStateEnum.CREATED) [
		// println("feature2feature " + feature)
		val leftfeature = feature
		val rightfeature = componentref.createChild(componentInstance_FeatureInstance, featureInstance)
		rightfeature.set(namedElement_Name, leftfeature.name)
		rightfeature.set(featureInstance_Category, leftfeature.category)
		rightfeature.set(featureInstance_Direction, leftfeature.direction)

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, leftfeature)
			addTo(aadl2AadlTrace_RightInstance, rightfeature)
		]
	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, feature, null).get

		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, feature, null).get

		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val connection2connection = createRule(Copy_connection.instance).action(CRUDActivationStateEnum.CREATED) [
		val connectionleft = connection
		val connectionright = componentref.createChild(
			componentInstance_ConnectionInstance,
			connectionInstance
		)

		connectionright.set(namedElement_Name, connection.name)
		connectionright.set(connectionInstance_Kind, connection.kind)
		connectionright.set(connectionInstance_Complete, connection.complete)

		connectionright.set(connectionInstance_Source, sourceref)
		connectionright.set(connectionInstance_Destination, destinationref)

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, connectionleft)
			addTo(aadl2AadlTrace_RightInstance, connectionright)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, connection, null).get
		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [

		val traceMatch = engine.is_in_trace_for_copy.getAllValuesOftrace(null, connection, null)
		for (var i = 0; i < traceMatch.length; i++) {
			val trace = traceMatch.get(i) as Aadl2AadlTrace
			val element = trace.rightInstance.head
			element.remove
			trace.remove
		}

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val connectionRef2connectionRef = createRule(Copy_connectionref.instance).action(CRUDActivationStateEnum.CREATED) [

		val connectionRefright = connectionref.createChild(
			connectionInstance_ConnectionReference,
			connectionReference
		)
//		connectionRefright.set(namedElement_Name, connectionR.name)
		connectionRefright.set(connectionReference_Source, sourceref)
		connectionRefright.set(connectionReference_Destination, destinationref)
		connectionRefright.set(connectionReference_Context, contextRref)

		val connectionRefleft = connectionR

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, connectionRefleft)
			addTo(aadl2AadlTrace_RightInstance, connectionRefright)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, connectionR, null).get
		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace_for_copy.getOneArbitraryMatch(aadl2aadl, null, connectionR, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	override setQueries() {
		return required_queries
	}

}
