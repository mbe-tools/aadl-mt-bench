package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.CopyingAadl2aadlTransformation;

public class CopyingUpdate extends AbstractViatraScenario {

	public CopyingUpdate() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_ATT;
	}

	@Override
	AbstractTransformation getSpecification() {
		return new CopyingAadl2aadlTransformation();
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_UPDATE_ATT_SUFFIX;
	}
}