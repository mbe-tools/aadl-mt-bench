package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.Aadl2aadlTransformation;

public class RefinementAddition extends AbstractViatraScenario {

	public RefinementAddition() {
		super();
	}

	@Override
	ScenarioKind getScenario() {

		return ScenarioKind.ADDITION;
	}

	@Override
	AbstractTransformation getSpecification() {
		return new Aadl2aadlTransformation();
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_ADD_SUFFIX;
	}
}
