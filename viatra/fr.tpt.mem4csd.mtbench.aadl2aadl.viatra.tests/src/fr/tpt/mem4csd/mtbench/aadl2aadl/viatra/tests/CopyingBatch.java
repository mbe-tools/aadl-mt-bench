package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.CopyingAadl2aadlTransformation;

public class CopyingBatch extends AbstractViatraScenario {

	public CopyingBatch() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.BATCH;
	}

	@Override
	AbstractTransformation getSpecification() {
		return new CopyingAadl2aadlTransformation();
	}
	
	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_BATCH_SUFFIX;
	}

}
