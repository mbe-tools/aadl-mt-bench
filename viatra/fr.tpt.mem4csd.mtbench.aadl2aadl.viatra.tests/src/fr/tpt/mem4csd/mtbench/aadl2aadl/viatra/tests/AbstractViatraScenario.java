package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.viatra.query.patternlanguage.emf.EMFPatternLanguageStandaloneSetup;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.emf.EMFScope;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ModelModif;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ResourcesUtil;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.Result;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec;
import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2aadlPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;

public abstract class AbstractViatraScenario extends AbstractScenario {

	private static final String BASE_RESOURCES_AADL2AADL_MODELS = "resources/traces/";

	protected AbstractViatraScenario() {
		super();
	}

	@Override
	protected void setup() {
		Aadl2aadlPackage.eINSTANCE.eClass();
		new EMFPatternLanguageStandaloneSetup().createInjectorAndDoEMFRegistration();

		super.setup();
	}

	abstract AbstractTransformation getSpecification();

	abstract ScenarioKind getScenario();

	abstract String getExtension();

	protected long[] scenarioSteps(ScenarioKind scenarioKind, Resource leftSystem, ModelModif modif, int runtimes,
			AbstractTransformation viatraTransformation) {
		long t0, t1;
		long[] runs = new long[runtimes];

		if (scenarioKind != ScenarioKind.BATCH) {
			viatraTransformation.execute();
		}
		for (int i = 0; i < runtimes; i++) {
			modif.applyModif(scenarioKind, leftSystem);
			t0 = System.nanoTime();
			viatraTransformation.execute();
			t1 = System.nanoTime();
			runs[i] = t1 - t0;
		}
		return runs;
	}

	@Override
	public Resource executeScenario(final int testNumber, final Result res, final String inputModelName)
			throws Exception {
		long[] runsOfBatch = new long[res.getRuntimes()];
		ModelModif modif = new ModelModif();
		ScenarioKind scenario = getScenario();
		Resource aadlTraceResource = loadTraceInputResource(inputModelName + getExtension());

		Aadl2AadlTraceSpec aadlTraceModel = (Aadl2AadlTraceSpec) aadlTraceResource.getContents().get(0);
		System.out.println("Test " + testNumber + " Performing transformation:");
		clear(aadlTraceModel);

		long t0, t1;
		t0 = System.nanoTime();
		final ViatraQueryEngine engine = ViatraQueryEngine
				.on(new EMFScope(aadlTraceModel.eResource().getResourceSet()));
		final AbstractTransformation transformation = getSpecification();
		transformation.initialize(aadlTraceModel, engine);
		t1 = System.nanoTime();

		System.out.println("Init " + testNumber + " in " + Long.toString(t1 - t0) + " ns");

		runsOfBatch = scenarioSteps(scenario, aadlTraceModel.getLeftSystem().eResource(), modif, res.getRuntimes(),
				transformation);

		transformation.dispose();
		res.setExecutionTimes(inputModelName, testNumber, res.getMedian(runsOfBatch));
		System.out.println("Test " + testNumber + " in " + res.getMedian(runsOfBatch) + " ns");

		aadlTraceModel.getRightSystem().eResource().save(null);
		aadlTraceResource.save(null);

		System.out.println("Transformation finished.");

		return aadlTraceModel.getRightSystem().eResource();
	}

	protected Resource loadTraceInputResource(final String modelName) {
		final URI modelUri = ResourcesUtil.createURI(BASE_RESOURCES_AADL2AADL_MODELS + modelName,
				Aadl2aadlPackage.eNAME);

		return ResourcesUtil.createResourceSet().getResource(modelUri, true);
	}

	protected void clear(Aadl2AadlTraceSpec aadlmodel) {
		if (!aadlmodel.getRightSystem().eContents().isEmpty()) {
			EcoreUtil.deleteAll(aadlmodel.getRightSystem().eContents(), true);
		}

		if (!aadlmodel.getTraces().isEmpty()) {
			aadlmodel.getTraces().clear();
		}

		try {// save traces
			aadlmodel.eResource().save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try { // save refined model
			aadlmodel.getRightSystem().eResource().save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void clearAll(String extension) {
		for (int testNum = 0; testNum < 15; testNum++) {
			URI modelUri = ResourcesUtil.createURI(
					BASE_RESOURCES_AADL2AADL_MODELS + BenchmarkConstants.TEST_CASES[testNum] + extension,
					Aadl2aadlPackage.eNAME);
			ResourceSet resSet = ResourcesUtil.createResourceSet();
			Resource aadlTraceResource = resSet.getResource(modelUri, true);
			Aadl2AadlTraceSpec aadlTraceModel = (Aadl2AadlTraceSpec) aadlTraceResource.getContents().get(0);
			clear(aadlTraceModel);
		}

	}

	protected void clearRightSystem(Aadl2AadlTraceSpec aadlmodel) {
		if (aadlmodel.getRightSystem() != null) {
			final Resource rightSysRes = aadlmodel.getRightSystem().eResource();
			EcoreUtil.delete(aadlmodel.getRightSystem(), true);
			rightSysRes.unload();
			rightSysRes.getResourceSet().getResources().remove(rightSysRes);
		}
		aadlmodel.getTraces().clear();
	}
}
