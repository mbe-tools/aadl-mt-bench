
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.Aadl2aadlTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;

public class RefinementUpdateReference extends AbstractViatraScenario {

	public RefinementUpdateReference() {
		super();
	}

	@Override
	AbstractTransformation getSpecification() {
		return new Aadl2aadlTransformation();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_REF;
	}
	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_UPDATE_REF_SUFFIX;
	}

}
