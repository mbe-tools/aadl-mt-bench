package fr.tpt.mem4csd.utils.compare.text;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import fr.tpt.mem4csd.utils.compare.DefaultComparisonReport;
import fr.tpt.mem4csd.utils.compare.IComparator;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;

public class DefaultComparatorText implements IComparator {

	private final Collection<String> ignoredFileExtensions;

	public DefaultComparatorText() {
		this( Collections.<String>emptyList() );
	}

	public DefaultComparatorText( final Collection<String> p_ignoredFileExtensions ) {
		super();
		
		ignoredFileExtensions = p_ignoredFileExtensions;
	}

	@Override
	public IComparisonReport compare(	final File fileLeft,
										final File fileRight )
	throws IOException {
		return compare( fileLeft, fileRight, null );
	}

	@Override
	public IComparisonReport compare(	final File fileLeft,
										final File fileRight,
										final FilenameFilter filesFilter )
	throws IOException {
		return compare( fileLeft, fileRight, filesFilter, false );
	}

	@Override
	public IComparisonReport compare(	final File fileLeft,
										final File fileRight,
										final FilenameFilter filesFilter,
										final boolean ignoreExtraFilesRightSide )
	throws IOException {
		if ( fileLeft.isDirectory() ) {
			if ( fileRight.isDirectory() ) {
				final IComparisonReport compoundReport = new Report( fileLeft, fileRight );
				compareDirs( fileLeft, fileRight, filesFilter, ignoreExtraFilesRightSide, true, compoundReport );
				
				if ( !ignoreExtraFilesRightSide ) {
					compareDirs( fileRight, fileLeft, filesFilter, ignoreExtraFilesRightSide, false, compoundReport );
				}
				
				return compoundReport;
			}

			return createComparisonReport( 	false,
											"File Left '" + fileLeft + "' is a directory while file Right '" + fileRight + "' is not." );
		}

		if ( fileRight.isDirectory() ) {
			return createComparisonReport( 	false,
											"File Left '" + fileLeft + "' is not a directory while file Right '" + fileRight + "' is." );
		}
		
		if ( ignore( fileLeft, filesFilter ) ) {
			return createComparisonReport( 	true,
											"File '" + fileLeft + "' is ignored in this comparison." );
		}
		
		if ( ignore( fileRight, filesFilter ) ) {
			return createComparisonReport(	true,
											"File '" + fileRight + "' is ignored in this comparison." );
		}

		return new TextDiff().compare( fileLeft, fileRight );
	}
	
	private void compareDirs( 	final File leftDir,
								final File rightDir,
								final FilenameFilter filesFilter,
								final boolean ignoreExtraFilesRightSide,
								final boolean compareContent,
								final IComparisonReport compoundReport )
	throws IOException {
		assert leftDir.isDirectory() && rightDir.isDirectory();
		final File[] rightFiles = rightDir.listFiles();
		
		for ( final File leftDirFile : leftDir.listFiles() ) {
			final String leftDirFileName = leftDirFile.getName();
			boolean found = false;
			
			for ( final File rightDirFile : rightFiles ) {
				if ( leftDirFileName.equals( rightDirFile.getName() ) ) {
					if ( compareContent ) {
						compoundReport.getSubReports().add( compare( leftDirFile, rightDirFile, filesFilter, ignoreExtraFilesRightSide ) );
					}
					
					found = true;
					
					break;
				}
			}
			
			if ( !found ) {
				compoundReport.getSubReports().add( createComparisonReport( false,
																			"File '" + leftDirFile.getName() + "' of directory '" + leftDir + "' is not contained in directory '" + rightDir + "'." ) );
			}
		}
	}
	
	protected IComparisonReport createComparisonReport( final boolean p_equals,
														final String p_message ) {
		return new DefaultComparisonReport( p_equals, p_message );
	}
	
	protected boolean ignore( 	final File file,
								final FilenameFilter filesFilter ) {
		final boolean filtered;
		
		if ( filesFilter == null ) {
			filtered = false;
		}
		else {
			filtered = !filesFilter.accept( file.getParentFile(), file.getName() );
		}
			
		if ( filtered ) {
			return true;
		}
		
		for ( final String ext : ignoredFileExtensions ) {
			if ( file.getName().endsWith( ext ) ) {
				return true;
			}
		}
		
		return false;
	}
}
