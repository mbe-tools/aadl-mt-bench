package fr.tpt.mem4csd.utils.compare.text;

/**
 * Delete a block from the old file.
 */
public class DeleteCommand extends EditCommand {
	
    public DeleteCommand( final FileInfo oldFileInfo ) {
        super();
        
        command = "Delete";
        oldLines = oldFileInfo.nextBlock();
        oldLines.reportable = true;
    }
}
