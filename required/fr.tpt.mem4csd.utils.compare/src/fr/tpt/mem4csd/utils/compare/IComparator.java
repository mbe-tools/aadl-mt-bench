package fr.tpt.mem4csd.utils.compare;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public interface IComparator {
	
	IComparisonReport compare(	File fileLeft, 
								File fileRight ) 
	throws IOException;

	IComparisonReport compare(	File fileLeft, 
								File fileRight,
								FilenameFilter filesFilter ) 
	throws IOException;

	IComparisonReport compare(	File fileLeft, 
								File fileRight,
								FilenameFilter filesFilter,
								boolean ignoreExtraFilesRightSide ) 
	throws IOException;
}
