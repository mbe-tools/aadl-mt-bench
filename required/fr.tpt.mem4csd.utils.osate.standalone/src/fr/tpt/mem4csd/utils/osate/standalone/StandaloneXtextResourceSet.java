package fr.tpt.mem4csd.utils.osate.standalone;


import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.xtext.resource.ClasspathUriResolutionException;
import org.eclipse.xtext.resource.ClasspathUriUtil;
import org.eclipse.xtext.resource.XtextResourceSet;

public class StandaloneXtextResourceSet extends XtextResourceSet {
	
	public static final String SCHEME = "classpath:/";
	
	private URI convertURItoClasspath(URI uri) {		
		if (uri.scheme() != null && uri.isPlatformPlugin()) {
			URI resultURI = URI.createURI(SCHEME);
			int i = 0;
			
			for(String segment: uri.segmentsList()) {
				if(i <= 1) {
					i++;
					continue;
				}	
				resultURI = resultURI.appendSegment(segment);
			}
			return resultURI;
		}
		
		return uri;
	}
	
    private URI resolveClasspathURI(URI uri) {
        return getClasspathUriResolver().resolve(getClasspathURIContext(), uri);
    }
	
	@Override
	public Resource getResource(URI uri, boolean loadOnDemand) {
		
		uri = convertURItoClasspath(uri);
		
		return super.getResource(uri, loadOnDemand);
	}
	
	@Override
    public URIConverter getURIConverter() {
        if (uriConverter == null) {
            uriConverter = new ExtensibleURIConverterImpl() {
            	@Override
            	public boolean exists(URI uri, Map<?, ?> options) {
            		String before = uri.toString();
					uri = convertURItoClasspath(uri);
					if (!before.equals(uri.toString()))
						{
							System.err.println("before = " + before);
								System.err.println("after = " + uri.toString());
						}
                	options = addTimeout(options);
            		return super.exists(uri, options);
            	}
                @Override
                public URI normalize(URI uri) {
                	URI normalizedURI = normalizationMap.get(uri);
                	if (normalizedURI != null) {
                		return normalizedURI;
                	}
                	if (ClasspathUriUtil.isClasspathUri(uri)) {
                        URI result = StandaloneXtextResourceSet.this.resolveClasspathURI(uri);
                        if (ClasspathUriUtil.isClasspathUri(result))
                        	throw new ClasspathUriResolutionException(result);
                        result = super.normalize(result);
                        return result;
                    }
                    return super.normalize(uri);
                }
                
				@Override
                public InputStream createInputStream(URI uri, Map<?, ?> options) throws IOException {
					// timeout is set here because e.g. SAXXMIHandler.resolveEntity(String,String) calls it without a timeout
					// causing the builder to wait too long...
					uri = convertURItoClasspath(uri);
                	options = addTimeout(options);
                	return super.createInputStream(uri, options);
                }
				
				@Override
				public Map<String, ?> contentDescription(URI uri, Map<?, ?> options) throws IOException {
					uri = convertURItoClasspath(uri);
					options = addTimeout(options);
					return super.contentDescription(uri, options);
				}

            };
        }
        return uriConverter;
    }

}
