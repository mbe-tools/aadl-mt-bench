package fr.tpt.mem4csd.utils.compare.emf;

import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.DifferenceSource;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;

import fr.tpt.mem4csd.utils.compare.DefaultComparisonReport;

public class ComparisonReportEMF extends DefaultComparisonReport {
	
	private final ILabelProvider labelProvider;
	
	private final List<Diff> differences;
	
	public ComparisonReportEMF( final Resource p_leftResource,
								final Resource p_rightResource,
								final List<Diff> p_differences ) {
		super( 	p_differences.isEmpty(), 
				( p_differences.isEmpty() ? "No differences found" : p_differences.size() + " differences found" ) + 
				" between model '" + p_leftResource.getURI() +
				"' and model '" + p_rightResource.getURI() +
				"'" +
				( p_differences.isEmpty() ? "." : "!!!" ) );
		
		differences = p_differences;

		labelProvider = createLabelProvider();
	}
	
	protected ILabelProvider createLabelProvider() {
		final ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE );
		
		// The registry will not contain the adapters so must be provided manually
		if ( !Platform.isRunning() ) {
			for ( final AdapterFactory factory : createAdditionalAdapterFactories() ) {
				adapterFactory.addAdapterFactory( factory );
			}
		}
		
		adapterFactory.addAdapterFactory( new ResourceItemProviderAdapterFactory() );
		adapterFactory.addAdapterFactory( new ReflectiveItemProviderAdapterFactory() );
		
		return new AdapterFactoryLabelProvider( adapterFactory );
	}
	
	protected List<AdapterFactory> createAdditionalAdapterFactories() {
		return Collections.emptyList();
	}

	@Override
	public boolean containsDiff() {
		return !differences.isEmpty();
	}

	@Override
	public String getMessage() {
		final StringBuilder message = new StringBuilder();
		
		if ( containsDiff() ) {
			message.append( "Differences are: " );
			
			for ( final Diff difference : differences ) {
				final EObject sourceParent;
				final EObject targetParent;
				final Match match = difference.getMatch();
				
				if ( difference.getSource() == DifferenceSource.LEFT ) {
					sourceParent = match.getLeft();
					targetParent = match.getRight();
				}
				else {
					sourceParent = match.getRight();
					targetParent = match.getLeft();
				}

				final Object sourceValue;
				final EStructuralFeature feature;

				if ( difference instanceof ReferenceChange ) {
					final ReferenceChange refChange = (ReferenceChange) difference;
					sourceValue = refChange.getValue();
					feature = refChange.getReference();
				}
				else if ( difference instanceof AttributeChange ) {
					final AttributeChange attChange = (AttributeChange) difference;
					sourceValue = attChange.getValue();
					feature = attChange.getAttribute();
				}
				else {
					sourceValue = difference.toString();
					feature = null;
				}

				final Object targetValue;
				
				if ( targetParent == null || feature == null ) {
					targetValue = null;
				}
				else {
					targetValue = targetParent.eGet( feature );
				}
				
				message.append( System.lineSeparator() );

//				if ( targetValue == null ) {
					
//					message.append( difference.getClass().getSimpleName() );
//					message.append( " difference is " );
//					message.append( sourceValue );
//				}
//				else {
					message.append( "Value " );
					
					if ( difference.getKind() != DifferenceKind.CHANGE ) {
						message.append( "'" + getText( sourceValue ) + "' " );
					}
					
					message.append( "of feature '" );
					message.append( getText( sourceParent, feature ) );
					message.append( "' of " );
					message.append( getText( sourceParent ) );
					message.append( "' was " );
					
					switch ( difference.getKind() ) {
						case ADD:
							message.append( "added to the " );
							message.append( difference.getSource().getName().toLowerCase() );
							message.append( " side" );
							
							break;

						case CHANGE:
							message.append( "changed from '" );
							message.append( getText( sourceValue ) );
							message.append( "' to '" );
							message.append( getText( targetValue ) );
							message.append( "'" );
							
							break;
						case DELETE:
							if ( difference.getSource() == DifferenceSource.RIGHT ) {
								message.append( "removed from the left" );
							}
							else {
								message.append( "added to the right" );
							}
							
							message.append( " side" );
							
							break;
							
						case MOVE:
							message.append( "moved" );
							
							break;
		
						default:
							throw new IllegalArgumentException( "Unknowned difference kind: " + difference.getKind() );
					}
				//}
				
				message.append( "." );
			}
		}
		else {
			message.append( "EMF resources are identical." );
		}

		return message.toString();
	}
	
	protected String getText( 	final EObject sourceObject,
								final EStructuralFeature value ) {
		if ( sourceObject == null ) {
			return value.getEContainingClass() + labelProvider.getText( value );
		}
		
		return sourceObject.eClass().getName() + "." + labelProvider.getText( value );
	}
	
	protected String getText( final Object value ) {
		if ( value instanceof EStructuralFeature ) {
			return getText( (EStructuralFeature) value, null );
		}
		
		return labelProvider.getText( value );
	}
}
